#  Should be sourced.

#  bahelite.sh
#  Bash helper library for Linux to create more robust shell scripts.
#  © deterenkelt 2018–2020
#  https://github.com/deterenkelt/Bahelite
#

 # This work is based on the Bash Helper Library for Large Scripts,
#  that the author has been developing for Lifestream LLC in 2016,
#  and that was also licensed under the terms of GPL v3.
#

 # Bahelite is free software; you can redistribute it and/or modify it
#    under the terms of the GNU General Public License as published
#    by the Free Software Foundation; either version 3 of the License,
#    or (at your option) any later version.
#  Bahelite is distributed in the hope that it will be useful, but without
#    any warranty; without even the implied warranty of merchantability
#    or fitness for a particular purpose. See the GNU General Public License
#    for more details.
#

 # Bahelite doesn’t enable or disable any shell options, it leaves to the
#    programmer to choose an optimal set. Bahelite may only temporarily enable
#    or disable shell options – but only temporarily.
#  That said, it is highly recommended to use “set -feEu” in the main script,
#    and if you add -T to that, thus making the line “set -feEuT”, Bahelite
#    will be able to catch more bash errors.
#



                          #  Initial checks  #

 # Require bash v4.3 for declare -n.
#          bash v4.4 for the fixed typeset -p behaviour, ${param@x} operators,
#                    SIGINT respecting builtins and interceptable by traps,
#                    BASH_SUBSHELL that is updated for process substitution.
#
if	((
		${BASH_VERSINFO[0]:-0} <= 3  || (     ${BASH_VERSINFO[0]:-0} == 4
		                                   && ${BASH_VERSINFO[1]:-0} <  4
		                                )
	))
then
	echo "Bahelite error: bash v4.4 or higher required." >&2
	#  so that it would work for both sourced and executed scripts
	return 3  2>/dev/null || exit 3
fi

 # Sourced scripts are not allowed. (This is almost always a bad idea.)
#  In case you absolutely need to run Bahelite with a sourced script,
#  *and you are ready, that things will break,* you may export the
#  variable BAHELITE_LET_MAIN_SCRIPT_BE_SOURCED with any value.
#
if	   [ ! -v BAHELITE_LET_MAIN_SCRIPT_BE_SOURCED ]  \
	&& [ "${BASH_SOURCE[-1]}" != "$0" ]
then
	echo "Bahelite error: ${BASH_SOURCE[-1]} shouldn’t be sourced." >&2
	return 4
fi


                #  Cleaning the environment before start  #

 # Wipe user functions from the environment
#  This is done by default, because of the custom things, that often
#    exist in ~/.bashrc or exported from some higher, earlier shell. Being
#    supposed to only simplify the work in terminal, such functions may –
#    and often will – complicate things for a script.
#  To keep the functions exported to us in this scope, that is, the scope
#    where this very script currently execues, define BAHELITE_KEEP_ENV_FUNCS
#    variable before sourcing bahelite.sh. Keep in mind, that outer functions
#    may lead to an unexpected behaviour.
#  Aliases are not inherited through the environment, thankfully.
#
if [ ! -v BAHELITE_KEEP_ENV_FUNCS ]; then
	#  This wipes all functions, which names don’t start with an underscore
	#  (those that start with “_” or “__” are internal ones, mostly
	#  related to completion, and are harmless.)
	while read funcdef; do
		funcdef=${funcdef#declare }   #  strip “declare ”
		funcdef=${funcdef#* }         #  strip keys to declare
		[[ "$funcdef" =~ ^_ ]]  \
			|| unset -f "$funcdef"
	done < <(declare -F)
	unset funcdef
fi

 # The env command in shebang will not recognise -i, so an internal respawn
#    is needed in order to run the script in a totally clean environment.
#  But be aware, that “env -i” literally WIPES the environment – you won’t
#    find there neither $HOME nor $USER any more. If you wish simply to get
#    rid of globals from the mother script, simply use “env” (Bahelite over-
#    rides it in the “util_overrides” module for convenience.)
#
if	   [ -v BAHELITE_TOTAL_ENV_CLEAN ]  \
	&& [ ! -v BAHELITE_ENV_CLEANED   ]
then
	#  Re-execute the script in a new, clean environment
	exec /usr/bin/env -i BAHELITE_ENV_CLEANED=t  bash "$0" "$@"
	#  …and stop execution of the currently running script,
	#  as it was only a launchpad.
	exit $?
fi

declare -r BAHELITE_VARLIST_BEFORE_STARTUP="$(compgen -A variable)"



                       #  I. Bootstrap stage  #

                       #   Global variables   #

declare -grx BAHELITE_VERSION="5.1.3"
declare -grx BAHELITE_DIR=${BASH_SOURCE[0]%/*}


 # Specifies which modules to load and their options
#  (set it in your script, not here)
#  (set it /before/ you call bahelite.sh)
#
#  Syntax:
#      BAHELITE_LOAD_MODULES=(
#          module_name1
#          module_name2:optionA
#          module_name3:optionB,optionC=valueC,optionD
#      )
#      source /path/to/bahelite.sh
#
#  Module name corresponds to its file name without the “.sh” extension.
#    Core modules are loaded automatically (including bootstrap), the rest
#    must be mentioned explicitly.
#  Every module, except for the bootstrap modules, takes an option “help”,
#    that prints to console, what arguments that module takes. After process-
#    ing the “help” option of any module the execution stops.
#  Spaces and special characters are not allowed.
#
declare -ga BAHELITE_LOAD_MODULES


 # Modules may define startup jobs as elements in this array
#
#  When modules are loaded, they mostly just define functions, and if they
#  execute any code, it’s small and defines the necessary abstracts, things,
#  that don’t depend on anything. The functions, that organise facilities,
#  *may* depend on other functions-facility-organisers. These dependencies
#  are solved after loading modules. At the end of this script the functions
#  defined in this array, are called as “startup jobs” and their dependencies
#  are resolved then.
#
#  This array is to be used and expanded ONLY by Bahelite modules.
#  Item format: “func_name” or “func_name:after=another_funcname”.
#
declare -ga BAHELITE_POSTLOAD_JOBS=()


                      #  Main script variables  #

#  $0 == -bash if the script is sourced.
[ -f "$0" ] && {
	MYNAME=${0##*/}
	MYNAME_NOEXT=${MYNAME%.*}
	#  So that code in source’d modules could have the value of $0,
	#  as if it was running in the file called for execution.
	#  Used in  single_process_check()  in misc.sh.
	MYNAME_AS_IN_DOLLARZERO="$0"
	MYPATH=$(realpath --logical "$0")
	MYDIR=${MYPATH%/*}

	 # Used for desktop notifications in bahelite_messages_to_desktop.sh
	#  and in the title for dialog windows in bahelite_dialog.sh
	#
	[ -v MY_DISPLAY_NAME ] || {
		#  Not forcing lowercase, as there may be intended
		#  caps, like in abbreviations.
		MY_DISPLAY_NAME="${MYNAME_NOEXT^}"
	}

	 # Common name that lets a set of scripts use a single config directory,
	#    cache directory etc., while the scripts themselves may have different
	#    names.
	#  Having a globally defined variable removes the need to specify that
	#    common name as loading options to modules, that find/create common
	#    directories. See /modules/directories/source/any-source-dir.common.sh.
	#    As each script in a suite is supposed to have their own config file,
	#    the presence of MY_BUNCH_NAME automatically forces recognition by
	#    name in the rcfile module (a default “.rc.sh” or ”config.rc.sh” now
	#    won’t be loaded). MY_BUNCH_NAME doesn’t affect modules like “tmpdir”
	#    and “logging” etc., except when a module uses basic directories
	#   (e.g. logging, that typically depends on CACHEDIR).
	#
	[ -v MY_BUNCH_NAME ] && declare -rx MY_BUNCH_NAME

	ORIG_BASHPID=$BASHPID
	ORIG_PPID=$PPID

	ORIG_BASH_SUBSHELL=$BASH_SUBSHELL
}

declare -grx MYNAME  MYNAME_NOEXT  MYNAME_AS_IN_DOLLARZERO  MYPATH  MYDIR  \
             MY_DISPLAY_NAME  MY_BUNCH_NAME  ORIG_BASHPID  ORIG_PPID  \
             ORIG_BASH_SUBSHELL


                      #  Commandline arguments  #

declare -grx CMDLINE="$0 $*"

 # ARGS array is for the common use, and it may undergo changes, as the
#  main script would find necessary. A common change would happen when
#  the main script calls read_rcfile() from the rcfile module. It will read
#  a config file name (argument that ends on “.rc.sh”) and set it to the
#  RCFILE variable, then delete this item from the argument list.
#
declare -gx ARGS=("$@")

 # ORIG_ARGS is set once and for all, it will always have the list
#  of arguments as they were passed to the program. It should be relied
#  upon instead of ARGS, when the arguments need to be shown as the user
#  provided them, witheout any (pre)processing.
#
declare -grx ORIG_ARGS=("$@")



                      #  II. Loading modules  #

 # Loading bootstrap modules
#
#  During the bootstrap stage Bahelite is limited in the means to perform
#  self-reporting. To help that, it first loads modules that provide basic
#  or abridged functionality. See the README file in /modules/bootstrap
#  for details.
#

for module in 'read_module_options.sh'    \
              'terminal.sh'                \
              'messages_bootstrap.sh'       \
              'message_indentation.sh'       \
              'verbosity_base.sh'             \
              'checking_core_dependencies.sh'  \
              'load_modules.sh'
do
	source "$BAHELITE_DIR/modules/core/bootstrap/$module" || exit $?
done
unset module

#
#  Loading core modules and user-specified modules.
bahelite_load_modules
#
#  Checking modules’ dependencies.
check_required_utils



                #  III. Running modules’ postload jobs  #

bahelite_validate_postload_jobs "${BAHELITE_POSTLOAD_JOBS[@]}"

 # Before the main script starts, gather variables.
#  In case of an error this list would be compared to the other, created
#    before exiting, and the diff will be placed in "$LOGDIR/variables".
#  The list must be gathered /before/ postload jobs, for they may load
#    configuration files, journals from user’s config directory, and
#    after being uniq’ed at the end, in  bahelite_on_exit(),  these variables
#    would disappear, because they appeared too early. To make them always
#    get into the variable dump, the list here is gathered before running
#    postload jobs.
#
declare -r BAHELITE_VARLIST_AFTER_STARTUP="$(compgen -A variable)"

bahelite_run_postload_jobs "${BAHELITE_POSTLOAD_JOBS[@]}"

update_timeformat
(( ${VERBOSITY_CHANNELS[xtrace]} >= 3 ))  && builtin set -x


return 0
