#  Should be sourced.

#  load_modules.sh
#  Functions that determine, which modules have to be loaded, and load their
#  source code. Modules put the actual work in the functions and set them up
#  as “postload jobs” (there’s a separate module with the same name, that
#  calls them). However, the modules may have tiny parts of code, that is
#  executed at the time the module is loaded.
#  © deterenkelt 2018–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_LOAD_MODULES_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_LOAD_MODULES_VER='1.0.3'

vchan setup '!#Load_modules=2'


show_help_on_verbosity_channel_Load_modules() {
	cat <<-EOF
	Load_modules (self-report channel)
	    Describes the process of loading core and auxiliary modules with
	    their options. Core modules are loaded always, auxiliary – only if
	    specified in BAHELITE_LOAD_MODULES.
	    Levels:
	    2 – no additional output (the default);
	    3 – show a message when a module is going to be loaded; if “time-stat”
	        option was used on the module “bahelite”, also measure time that
	        the module loads;
	    4 – in addition, print the options for the module being loaded.

	Modules may create jobs that are postponed until the time, when all
	modules will be loaded. Set level 4 to “Postload_jobs” channel to see
	how those are running.
	EOF
	return 0
}


__show_usage_load_modules_module() {
	cat <<-EOF
	Bahelite module “load_modules” doesn’t take options!
	EOF
	return 0
}


for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_LOAD_MODULES[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_load_modules_module
		exit 0

	else
		redmsg "Bahelite: load_modules: unrecognised option “$opt”."
		__show_usage_load_modules_module
		exit 4
	fi
done
unset opt


 # Paths in which modules will be searched.
#
declare -gx BAHELITE_MODULE_PATHS=()



 # Find a module file and source it, passing it options
#  from BAHELITE_LOAD_MODULES.
#
#   $1     – module name.
#  [$2..n] – module parameters.
#
bahelite_load_module() {
	local module_name="$1"

	 # The module is to be sourced with the options as they are passed to this
	#    function. There is a nuance to the “source” builtin, however. If we’d
	#    simply extract the module options as arguments 2..n, and there would
	#    be none – by default no module receives any options – then the “source”
	#    builtin would substitute for the arguments to the sourced file the
	#    arguments passed to this very function, and it would pass the module
	#    name then.
	#  The ideally safe, stable and understandable solution to this is to shift
	#    the module name out of the arguments and use the rest as the arguments
	#    for the “source” command. This way even if it would expand to nothing,
	#    it couldn’t take anything from the function arguments.
	#
	shift
	#
	#  $@ are assigned to a named variable, because actually passing "$@"
	#  to the source builtin would cover by itself the double reading of $@,
	#  that would happen there.
	#
	local  module_opts=( "$@" )
	local  matching_modules_list=()
	local  matches_count=0
	local  module_path
	local  verbosity_depth=${VERBOSITY_CHANNELS[Load_modules]}
	local  module_short_name=${module_name##*/}

	module_short_name=${module_short_name%.sh}

	[ -v BAHELITE_MODULE_${module_short_name^^}_VER ] && {
		(( verbosity_depth >= 3 ))  \
			&& denied "Module ${module_short_name} is already loaded."
		return 0
	}
	(( verbosity_depth >= 3 ))  && {
		info "Loading module “$module_name”"
		milinc
		info "options: ${module_opts[*]}"
	}

	for module_path in "${BAHELITE_MODULE_PATHS[@]}"; do
		module_path+="$module_name.sh"
		[ -f "$module_path" -a  -r "$module_path" ] && {
			matching_modules_list+=( "$module_path" )
			let "matches_count++, 1"
		}
	done
	(( matches_count == 0 ))  && {
		redmsg "Bahelite error: cannot find module “$module_name”."
		return 4
	}
	(( matches_count > 1 ))  && {
		redmsg "Bahelite error: ambiguous module name “$module_name”:"
		for module_path in "${matching_modules_list[@]}"; do
			msg "  .${module_path##$BAHELITE_DIR}"  >&2
		done
		return 4
	}
	module_path="${matching_modules_list[0]}"

	(( verbosity_depth >= 4 ))  \
		&& info "Path: “$module_path”."

	 # As we are currently in a limited scope, the “source” command
	#  will make all declare calls local. To define global variables
	#  “declare -g” must be used in all modules!
	#
	source "$module_path"  "${module_opts[@]}"  || {
		redmsg "\nBahelite error: cannot load module “$module_name”."
		return 4
	}

	(( verbosity_depth >= 3 )) && mildec
	return 0
}
export -f bahelite_load_module


bahelite_load_modules() {
	local  module_name
	local  module_opts
	local  verbosity_depth=${VERBOSITY_CHANNELS[Load_modules]}

	local -a no_options=()

	(( verbosity_depth >= 3 ))  && {
		info "Bahelite: loading modules."
		milinc
	}

	#  First loading core modules, then the rest.
	for module_name in  util_overrides          \
	                    messages                 \
	                    verbosity                 \
	                    tmpdir                     \
	                    error_handling              \
	                    postload_jobs                \
	                    checking_module_dependencies  \
	                    "${!BAHELITE_LOAD_MODULES[@]}"; do

		 # Skip core module: it’s there only for options,
		#  and the options are already read.
		#
		[ "$module_name" = bahelite ] && continue

		 # If there were predefined options for the module, use them,
		#  and if there were none, make options point to an empty array.
		#  This is a paranoid avoidance to not forget to pass a module
		#  at least an empty string, for if there would be no options,
		#  the source command would pass the main script’s arguments to it.
		#
		[ -v BAHELITE_LOAD_MODULES[$module_name] ]  \
			&& declare -n module_opts=${BAHELITE_LOAD_MODULES[$module_name]}  \
			|| declare -n module_opts=no_options

		if [ $verbosity_depth -ge 3  -a  -v RUN_TIME_STATISTICS ]; then
			update_timeformat
			time bahelite_load_module $module_name "${module_opts[@]}" || return $?
		else
			bahelite_load_module $module_name "${module_opts[@]}" || return $?
		fi
		(( verbosity_depth >= 3 )) && echo
	done

	(( verbosity_depth >= 3 )) && mildec
	return 0
}
#  No export: init stage function.



 # Gathering module subdirectories to speed up search.
#
while IFS=$'\n' read -r; do
	BAHELITE_MODULE_PATHS+=( "$REPLY" )
done < <( builtin set +fT
          shopt -s globstar
          #  People like to override ls with options,
          #  so specifying it with an absolute path here.
          /bin/ls -d "$BAHELITE_DIR"/modules/**/
        )


return 0