#  Should be sourced.

#  message_indentation.sh
#  Allows to shift and remember the indentation level for output messages.
#  Especially handy in the scripts that are running one from within another.
#  © deterenkelt 2019–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MESSAGE_INDENTATION_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MESSAGE_INDENTATION_VER='1.3'

#  No verbosity channels: the “verbosity_base” module isn’t loaded yet.


__show_usage_message_indentation_module() {
	cat <<-EOF
	Bahelite module “message_indentation” options:

	ind_spaces=${__so}0…9${__s}
	    How much spaces to use for an indentation level. Must be a number
	    in range 1…9. The default is 2.

	inc=${__so}0…1${__s}
	    Turns on and off incremental indentation.

	    When set to 0, indentation goes in equal steps: each next level would
	    stand from the previous level on the same number of spaces (2 by de-
	    fault).

	    When set to 1, indentation receives 1 extra space per level: each
	    next level would stand from the previous on an increasing number
	    of spaces: base+0, base+1, base+2 and so on.

	    It’s disabled by default.

	inc_stop_at_level=${__so}0..99${__s}
	    Indentation stops getting incremented at the specified level
	    (and continues with ind_spaces per level).


	All option names are also accepted with hyphens in place of underscores.
	EOF
	return 0
}


for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_MESSAGE_INDENTATION[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_message_indentation_module
		exit 0

	elif [[ "$opt" =~ ^ind[_-]spaces=([1-9])$ ]]; then
		MSG_INDENTATION_SPACES_PER_LEVEL="${BASH_REMATCH[1]}"

	elif [[ "$opt" =~ ^inc=(0|1)$ ]]; then
		(( ${BASH_REMATCH[1]} == 1 ))  \
			&& MSG_INDENTATION_INCREMENTAL=t  \
			|| unset MSG_INDENTATION_INCREMENTAL

	elif [[ "$opt" =~ ^inc(_stop_at_|-stop-at-)level=([1-9]|[1-9][0-9])$ ]]; then
		MSG_INDENTATION_INCREMENTAL_STOP_AT_LEVEL=$opt

	else
		redmsg "Bahelite: message_indentation: unrecognised option “$opt”."
		__show_usage_message_indentation_module
		exit 4
	fi
done
unset opt


 # Checking, if it’s already set, in case one script calls another –
#  so that indentaion would be inherited in the inner script.
#
declare -gx MSG_INDENTATION_LEVEL

[ -v MSG_INDENTATION_LEVEL ]  \
	|| MSG_INDENTATION_LEVEL=0


 # So that mildrop() could decrease the level properly in chainloaded scripts.
#
declare -gx MSG_INDENTATION_LEVEL_UPON_ENTRANCE=$MSG_INDENTATION_LEVEL


 # The whitespace indentation itself.
#  As it belongs to markup, that user may use in the main script for custom
#    messages, it follows the corresponding style, akin to terminal sequences.
#  The string will be set according too the MSG_INDENTATION_LEVEL on the call
#    to __mi_assemble() below.
#
declare -gx __mi=''


 # Number of spaces to use per indentation level.
#  Though it’s possible to set tab width with “tput”, it’s better to only read
#  terminal capabilities, not alter them. (As that would unnecessarily inc-
#  rease the number of unobvious changes, that the main script should expect.)
#
declare -gx MSG_INDENTATION_SPACES_PER_LEVEL=2


 # The indentation may be assembled of equal steps, or there may be added
#  one extra space for each indendation level. This makes the levels more
#  distinctive in the output.
#  set – incremental steps (default)
#  unset – equal steps
#
declare -gx MSG_INDENTATION_INCREMENTAL


 # Where the incremental indentation should stop and from that level go
#    by the regular steps (specified with MSG_INDENTATION_SPACES_PER_LEVEL),
#    as if MSG_INDENTATION_INCREMENTAL isn’t set.
#  It was planned to make the incremental indentation go reverse at some
#    level, so that it would go +1… +2… +3… +2… +1… +2… +3… +2… +1… +2…
#    and then to repeat it, creating a catchy visual effect, helping the eye
#    through seeing differences, navigate faster. Nothing is less catchy than
#    something orderly, an even line.
#
#        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
#                 ^
#    Would you tell right away, which number by order is the letter C with
#    the pointer? Now try this:
#
#        CCCC
#            CCCC
#                CCCC
#                 ^  CCCC
#
#  This was supposed to help the eyekind of accordeon. But there was no time
#  to finish this. Whoever wishes to implement the idea, may use this example
#  of a number generator:
#
#      sw=1; for i in {0..24}; { echo $(( (i%4==0) && (sw=-sw), sw<0 ? i%4 : 4-(i%4) )); }
#
declare -gx MSG_INDENTATION_INCREMENTAL_STOP_AT_LEVEL=5


 # Dummy replacement until “util_overrides” module loads.
#
bahelite_xtrace_off() { return 1; }


 # Subroutine that checks if the verbosity level condition permits
#    the caller to change/set message indentation.
#  Supposed to be used only in the expression
#      __if_verbosity_permits || return 0
#  in the caller function.
#
__if_verbosity_permits() {
	#  If the verbosity module isn’t loaded yet, always permit. 
	[ -v BAHELITE_MODULE_VERBOSITY_VER ] && {
		local min_level=${FUNCNAME[2]}  # e.g. milinc, mildec, mildec-2 etc.
		min_level=${min_level#*-}
		[[ "$min_level" =~ ^([1-9]|[1-9][0-9]{0,1})$ ]]  || min_level=3

		(( $(vchan getlevel) >= $min_level ))  \
			&& return 0  \
			|| return 1
	}
	return 0
}
export -f __if_verbosity_permits

 # Counts factorial of a number passed as $1.
#
__fact() {
	local a="$1" sum=0
	for ((; a>0; sum+=a, a--)) { :; }
	echo $sum
}
export -f  __fact


 # Assembles __mi according to the current MSG_INDENTATION_LEVEL
#
__mi_assemble() {
	#  Internal! No xtrace_off/on needed!
	declare -g __mi=''
	local inc
	local i

	if	   [ -v MSG_INDENTATION_INCREMENTAL ]  \
		&& (( MSG_INDENTATION_LEVEL < MSG_INDENTATION_INCREMENTAL_STOP_AT_LEVEL ))
	then
		inc=$(__fact $((MSG_INDENTATION_LEVEL-1)) )
	else
		inc=0
	fi

	for	(( i=0;  i < (   MSG_INDENTATION_LEVEL
		               * MSG_INDENTATION_SPACES_PER_LEVEL
		               + $inc                              );  i++  ))
	do
		__mi+=' '
	done

	return 0
}
export -f  __mi_assemble


 # Increments the indentation level.
#  [$1] — number of times to increment $MI_LEVEL.
#         The default is to increment by 1.
#
__milinc() {
	#  Internal! No xtrace_off/on needed!
	__if_verbosity_permits || return 0
	local count=${1:-1}
	local z

	for ((z=0; z<count; z++)); do
		let '++MSG_INDENTATION_LEVEL,  1'
	done
	__mi_assemble || return $?
}
export -f  __milinc


 # Decrements the indentation level.
#  [$1] — number of times to decrement $MI_LEVEL.
#  The default is to decrement by 1.
#
__mildec() {
	#  Internal! No xtrace_off/on needed!
	__if_verbosity_permits || return 0
	local count=${1:-1}
	local z

	if (( MSG_INDENTATION_LEVEL == 0 )); then
		warn "No need to decrease indentation, it’s on the minimum."
		print_call_stack
	else
		for ((z=0; z<count; z++)); do
			let '--MSG_INDENTATION_LEVEL,  1'
		done
		__mi_assemble || return $?
	fi

	return 0
}
export -f  __mildec


 # Sets the indentation level to a specified number.
#  The use of this function is discouraged. milinc, mildec and mildrop are
#  better for handling increases and drops in the message indentation level.
#  $1 – desired indentation level, 0..9999.
#
__milset () {
	#  Internal! No xtrace_off/on needed!
	__if_verbosity_permits || return 0
	local mi_level=${1:-}

	[[ "$mi_level" =~ ^[0-9]{1,4}$ ]] || {
		warn "Indentation level should be an integer between 0 and 9999."
		return 0
	}
	MSG_INDENTATION_LEVEL=$mi_level
	__mi_assemble || return $?
}
export -f  __milset


 # Removes any indentation.
#
__mildrop() {
	#  Internal! No xtrace_off/on needed!
	__if_verbosity_permits || return 0
	MSG_INDENTATION_LEVEL=$MSG_INDENTATION_LEVEL_UPON_ENTRANCE
	__mi_assemble || return $?
}
export -f  __mildrop


 # Wrappers for the internal functions
#
#  Sets for verbosity levels 3 to 8. Like with similar alises to info() and
#  msg() in the “messages” module, you can create functions for levels above 8
#  by copying the code and changing numbers.
#
source <(
	for i in '' {4..8}; do
		cat <<-EOF
		milinc${i:+-$i}() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__milinc "\$@"
		}
		mildec${i:+-$i}() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__mildec "\$@"
		}
		milset${i:+-$i}() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__milset "\$@"
		}
		mildrop${i:+-$i}() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__mildrop "\$@"
		}
		export -f  milinc${i:+-$i}  \
		           mildec${i:+-$i}  \
		           milset${i:+-$i}  \
		           mildrop${i:+-$i}
		EOF
	done
)

__mi_assemble


return 0