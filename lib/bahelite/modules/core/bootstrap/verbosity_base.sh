#  Should be sourced.

#  verbosity_base.sh
#  Provides basic functionality for the bootstrap stage. To be extended
#  by the “verbosity” module.
#  © deterenkelt 2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_VERBOSITY_BASE_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_VERBOSITY_BASE_VER='1.0.1'

#  Verbosity channels are defined below function definitions.


__show_usage_verbosity_module() {
	cat <<-EOF
	Bahelite module “verbosity” doesn’t take options!
	But you may use the verbosity channel, for example with

	    $ VERBOSITY=Verbosity=3 ./$MYNAME

	or perhaps --help-verbosity command line option may help.
	EOF
	return 0
}

for opt in "${BAHELITE_LOAD_MODULES__OPTS_FOR_VERBOSITY[@]}"; do
	if [ "$opt" = help ]; then
		__show_usage_verbosity_module
		exit 0

	else
		redmsg "Bahelite: verbosity: unrecognised option “$opt”."
		__show_usage_verbosity_module
		exit 4
	fi
done
unset opt


 # Which level should newly created verbosity channels use, if it wouldn’t
#  be specified. Defaults to 3, as the channels created in the main script
#  are supposed to mirror the “main” channel behaviour.
#
declare -gx VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS=3



show_help_on_verbosity() {
	cat <<-EOF

	Verbosity channels

	To make the script more quiet or verbose, set levels to verbosity
	channels. It works the same way as with the environment variable:

	    $ VERBOSITY=channelA=3[,channelB=5][,…] ./$MYNAME

	as with the commandline parameter:

	    $ ./$MYNAME --verbosity channelA=3[,channelB=5][,…]

	There are many verbosity channels, and some of them handle software
	outputs (like to console or to a log file) while the other serve only
	as a gateway to warning/info/verbose messages. To see the list of core
	channels, try

	    $ VERBOSITY=Verbosity=3 ./$MYNAME

	Some helpful output channels are: “log”, “console”, “xtrace”, “tmpdir”.
	Among the message-controlling channels, look before all at “main”.
	To get information about a particular channel, run this script with

	    $ ./$MYNAME --help-verbosity=CHANNELNAME

	Self-report channels commonly use levels 3 to 5. Channels, that
	gate an output, typically use the range 0–3.
	EOF

	return 0
}


 # Validates verbosity string specified via VERBOSITY environment variable
#  or via the commandline option --verbosity.
#
#  $1 – a string specifying channels and the levels they should use.
#       The format is “channel=level[,channel2=level2,…]”.
#       “channel” may be a group name (but only the name, without the list
#       of function names that comprise that group).
#
__validate_verbosity() {
	local verbosity="$1"
	local chan_name='[A-Za-z_][A-Za-z0-9_-]*'
	local level='([0-9]|[1-9][0-9]{0,4})'
	local unit="$chan_name=$level"

	[[ "$verbosity" =~ ^($unit)(,$unit)*,?$ ]]  || {
		redmsg "Invalid specification of verbosity: “$verbosity”."
		redmsg "Run the script with --help-verbosity to print the format."
		err-nostack 'Cannot apply verbosity setting.'
	}

	return 0
}


 # Reads channel=level specifiers from $VERBOSITY and assigns them
#  to VERBOSITY_CHANNELS.
#
#  $1 – a string with verbosity specifiers, see the format
#       in __validate_verbosity().
#
__set_verbosity_to_channels() {
	declare -gAx VERBOSITY_CHANNELS
	declare -gAx VERBOSITY_CHANNELS_SUSPICIOUS

	local verbosity="$1"
	local verbosity_as_array=(  ${verbosity//,/ }  )
	local item
	local ch_name
	local ch_level

	for item in "${verbosity_as_array[@]}"; do
		ch_name=${item%=*}
		ch_level=${item#*=}
		VERBOSITY_CHANNELS[$ch_name]=$ch_level
		#  Mark all names set from the command line or environment
		#  as suspicious. vchan() will be taking these marks off one by one.
		VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name]="$ch_name"
	done

	return 0
}


 # Sets VERBOSITY according to --verbosity or --quiet. Verbosity levels
#  specified with these options take priority over the specification in the
#  environment variable VERBOSITY.
#
#  $1..n – positional arguments to the main script, usually
#          passed as "${ARGS[@]}".
#
#  Changes $ARGS.
#
__extract_verbosity_from_args() {
	decalre -gx VERBOSITY
	decalre -gx ARGS

	local  temp_args=( "$@" )
	local  i
	local  arg
	local  nextarg
	local  number_of_deleted_args
	local  args_to_unset=()

	for ((i=0; i<${#temp_args[*]}; i++)); do
		unset -n  arg || true  # Sic!
		declare -n  arg="temp_args[$i]"

		(( i < (${#temp_args[*]}-1) ))  && {
			declare -n nextarg="temp_args[$i+1]"
		}

		[ "$arg" = '--quiet' ] && {
			VERBOSITY=log=0,console=0,desktop=0,xtrace=0,bahelite=0,main=0
			args_to_unset+=(  $(( i - number_of_deleted_args++ ))  )
			continue
		}

		[[ "$arg" =~ ^--verbosity$ ]] && {
			if  (( i < (${#temp_args[*]}-1) ));  then
				__validate_verbosity "$nextarg"
				VERBOSITY="$nextarg"
				args_to_unset+=(  $((   i - number_of_deleted_args   ))
				                  $(( i+1 - number_of_deleted_args++ ))  )
				let '++i,  1'
				continue

			else
				err-nostack '--verbosity needs an argument.'
			fi
		}


		[[ "$arg" =~ ^--verbosity=(\'|\"|)(.+)(\'|\"|)$ ]]  && {
			__validate_verbosity "${BASH_REMATCH[2]}"
			VERBOSITY="${BASH_REMATCH[2]}"
			args_to_unset+=(  $(( i - number_of_deleted_args++ ))  )
			continue
		}
	done

	for i in ${args_to_unset[*]}; do
		unset temp_args[$i]
	done

	(( ${#temp_args[*]} != ${#ARGS[*]} )) && {
		ARGS=( "${temp_args[@]}" )

		(( ${VERBOSITY_CHANNELS[Verbosity]} >= 3 )) && {
			info "Setting VERBOSITY to “$VERBOSITY”."
			info "Setting new ARGS:"
			milinc
			for ((i=0; i<${#ARGS[@]}; i++)) do
				msg "ARGS[$i] = ${ARGS[i]}"
			done
			mildec
		}

	}

	return 0
}
#  No export: init stage function.


 # CRUDE implementation of vchan(), that is specifically made simplified
#  to accomodate the MINIMAL working facility for the bootstrap stage.
#  (This function is overloaded later, when the full-fledged version
#  of the modules loads.)
#
#  $1 – vchan command.
#  $2..n – command parameters.
#
#  Commands and their arguments:
#    - minlevel  "<channel_name>" "<minimum_level>"
#      compares the verbosity level of the specified channel with the speci-
#      fied number. Returns 0, if the the channel’s level would be equal or
#      greater than the specified one, returns 1 otherwise.
#    - setup "[!][#]<channel_name>[=<level>]"
#      provides an early means to create or update a channel. As the channel
#      flags (attributes) can be set only when the full-fledged implementation
#      is loaded, setting them is palced as a postponed job.
#
#  This implementation is only for the bootstrap modules!
#
#  Example of use:
#      vchan minlevel console 3 && { info "… … …"; }
#
#  At the bootstrap stage it is crucial to avoid running subshells, as the
#  “error_handling” and “messages” modules, that deal with them properly,
#  are yet to be loaded. Thus implementing bootstrap checks for verbosity like
#      (( $(vchan getlevel <channel_name>) >= 5 )) && {
#          …
#      }
#  would be prone to spawning issues. Implementing the “minlevel” subcommand
#  instead allows to avoid using a subshell.
#
#  Things to keep in mind about the bootstrap stage:
#    - channels may be already set, if VERBOSITY or --verbosity was used;
#    - until the full-fledged “verbosity” module loads, channels do not
#      have attributes. This simplifies this implementation, but requires
#      to remember attributes and fire up a postload job, that would set
#      those attributes using another, full-fledged version of vchan().
#
vchan() {
	local  command="${1:-}"
	local  ch_name
	local  min_level
	local  ch_spec
	local  ch_meta
	local  ch_locklevel_spec
	local  ch_level
	local  known_commands=( minlevel setup )
	local  name_pat='[A-Za-z_][A-Za-z0-9_-]*'
	local  level_pat='([0-9]|[1-9][0-9]{0,4})'
	local  vertbar='[[.vertical-line.]]'

	[[ "$command" =~ ^($(IFS='|'; echo "${known_commands[*]}"))$ ]]  \
		|| err "vchan (bootstrap): Unknown command: “$command”."

	case "$command" in
		minlevel)
			ch_name="${2:-}"
			[[ "$ch_name" =~ ^$name_pat$ ]]  \
				|| err "vchan: Impossible channel name: “$ch_name”."
			[ -v VERBOSITY_CHANNELS[$ch_name] ]  \
				|| err "vchan: Channel “$ch_name” doesn’t exist."
			min_level="${3:-}"
			[[ "$min_level" =~ ^$level_pat$ ]]  \
				|| err "vchan: Wrong minimal level: “$min_level”. Level must be a number."
			ch_level=${VERBOSITY_CHANNELS[$ch_name]}
			(( ch_level >= min_level ))  \
				&& return 0  \
				|| return 1
			;;

		setup)
			ch_spec="${2:-}"
			#    Extra parentheses from level_pat!  ↖↘                  ↖↘
			if [[ "$ch_spec" =~ ^(\*?!?#?)($name_pat)(=($level_pat)|)(\|!?$level_pat|)$ ]]; then
				ch_meta="${BASH_REMATCH[1]}"
				ch_name="${BASH_REMATCH[2]}"
				ch_level="${BASH_REMATCH[4]}"
				ch_locklevel_spec="${BASH_REMATCH[6]}"
				[ "$ch_level" ]  \
					|| ch_level=$VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS
				source <(
					cat <<-EOF
					set_chflags_for_$ch_name() {
						vchan setup "$ch_meta$ch_name$ch_locklevel_spec"
					}
					EOF
				)
				BAHELITE_POSTLOAD_JOBS+=(
					set_chflags_for_$ch_name
				)

			else
				err "vchan: Wrong specification for a channel: “$ch_spec”."
			fi

			[ -v VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name] ]  \
				&&  unset VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name]

			[ -v VERBOSITY_CHANNELS[$ch_name] ]  && {
				#  Channel exists. Probably created via environment variable
				#  or command line parameter – it’s a user setting (and the
				#  level – too).
				return 0
			}
			VERBOSITY_CHANNELS[$ch_name]=$ch_level
			;;
	esac
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_Verbosity() {
	cat <<-EOF
	Verbosity (self-report channel)
	    Levels:
	    2 – no additional output (the default)
	    3 – list verbosity channels on exit
	    4 – report on channel creation/alternation
	    5 – report in details about how channel attributes change during
	        alternation.
	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_console() {
	cat <<-EOF
	console (output channel)
	    Controls output to console.
	    Levels:
	    0 – stderr and stdout are disabled.
	    1 – only stderr is enabled.
	    2 – both stderr and stdout are enabled (the default).
	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_xtrace() {
	cat <<-EOF
	xtrace (output channel)
	    Controls the debugging output (what \`set -x\` enables).
	    Levels:
	    0 – no debugging output, “set -x” commands in the code are not
	        having effect (default).
	    1 – “set -x” commands in the code are allowed to have an effect.
	        (Your end users should never see debugging output by default.
	         Even if you didn’t simply forget “set -x” in the code and pla-
	         ced it intentionally, there must be a switch to turn this on.)
	    2 – xtrace output is redirected to its own log file. (Placed in the
	        LOGDIR and named the same as LOGFILE, but with an “.xtrace.log”
	        extension.)
	    3 – xtrace is enabled automatically once bahelite loads its modules
	        and before the execution returns to the main script.
	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_bahelite() {
	cat <<-EOF
	bahelite (output/self-report channel)
	    Controls the information coming from the core of this library.
	    Levels:
	    0 – no additional output (the default).
	    1 – few informational messages.
	    2 – xtrace can continue into calls to Bahelite library functions
	        (normally xtrace is temporarily shut down to reduce the output
	        to main script’s code).
	EOF
	return 0
}
#  No export: init stage function.


show_help_on_verbosity_channel_main() {
	cat <<-EOF
	main (self-report channel, generic channel for all messages)
	    Controls the verbosity of the main script’s,
	    i.e. $MYNAME’s messages.
	    Levels:
	    0 – all messages disabled.
	    1 – only error-level messages.
	    2 – only errors and warnings.
	    3 – all messages – errors, warnings and informational – are allowed
	        (the default).
	    4..N – turns on verbose messages.
	EOF
	return 0
}
#  No export: init stage function.



                        #  Verbosity channels  #

#  The introduction can be found in the full-fledged version
#  of the module, “verbosity.sh”, at the bottom. See also vchan() there.

declare -gAx VERBOSITY_CHANNELS=()


                        #  Channel attributes  #

 # If a channel has “meta” attribute set, then the channel name cannot match
#  function names in the call stack (it cannot become their current verbosity
#  channel). Library channels are all “meta”, except for the “main”, that is
#  the common, catch-em-all verbosity channel for the main script.
#
declare -gAx VERBOSITY_CHANNELS_META=()


 # Purely cosmetic indicator of whether the channel gates some kind of soft-
#  ware output (log, console, desktop messages, xtrace debugging channel…)
#  or it simply gates messages, i.e. verbosity per se (err, warn, info…).
#  This indicator is used only when listing channels to user.
#
#  Output channels are created only by this library – if the main script
#  creates some channels, it does this only to control messages. So this
#  indicator is a purely internal feature and setting it isn’t documented
#  in “vchan setup”.
#
declare -gAx VERBOSITY_CHANNELS_GATES_AN_OUTPUT=()


 # If a channel must span over two or more functions, their names are made
#  into channels, that duplicate their parent channel’s verbosity level.
#
declare -gAx VERBOSITY_CHANNELS_GROUP_ITEMS=()


 # For some channels setting a level is possible only once – i.e. when
#  disabling console output, this action is irreverisble. So to prevent
#  an attempt to set the level, when it would be futile, some channels
#  (primarily those that gate outputs) have an attribute, that points
#  at the level, reaching which the changes become irreversible.
#
declare -gAx VERBOSITY_CHANNELS_LOCK_ON_LEVEL=()


 # The channel system is flexible in that it allows “lazy”, postponed module
#  load. The main script may load optional modules (with more verbosity chan-
#  nels) after Bahelite would finish loading. Also the main script may define
#  channels, about which this library cannot possibly know.
#
#  Because the list of channels is never known beforehand, any channel is
#  allowed to be specified on the command line (or via environment). Speci-
#  fying a channel like that (pre-)creates it. But a human typing channel
#  names into VERBOSITY variable may mistype something, and then wonder why
#  some output isn’t there.
#
#  To highlight possibly mistyped channel names, Bahelite remembers each name
#  given from the command line (or via envirnment) and puts it into this array
#  thus marking the name as “suspicious”. When the modules load, they one by
#  one would be nudging the channel settings with vchan() and then vchan()
#  would take off the “suspicious” mark. Whatever channels would be left
#  marked, will show up in the “vchan list” output with a bright red ‘?’.
#
declare -gAx VERBOSITY_CHANNELS_SUSPICIOUS=()



                        #  Setting verbosity  #

 # First, from the environment variable
#
[ -v VERBOSITY ]  \
	&& __validate_verbosity "$VERBOSITY"  \
	&& __set_verbosity_to_channels "$VERBOSITY"
#
#
#  Then, from the commandline options
#
[[ "${ARGS[*]}"  =~ ^(|.+[[:blank:]])(--verbosity|--quiet)(.+[[:blank:]]|)$ ]]  \
	&& __extract_verbosity_from_args "${ARGS[@]}" \
	&& __validate_verbosity "$VERBOSITY"  \
	&& __set_verbosity_to_channels "$VERBOSITY"


 # Basic channels
#
#  Output channel. Meta-channel (cannot match function name).
vchan setup	'*!#console=2|!0'
#
#  Output channel. Meta-channel.
vchan setup '*!#xtrace=0|!2'
#
#  General self-report channel for the main script (the one that’s in MYNAME).
#  Regular channel (it /can/ match function name). Corresponds to the “main”
#  function in FUNCNAME call stack, thus becomes the generic channel to catch
#  messages going out from all functions, that are without a channel.
vchan setup 'main=3'
#
#  Core module’s verbosity channel to mark the start and the end of the lib-
#  rary’s overall loading process. It’s also used for things not related to
#  any particular module and to many at once.
vchan setup '*!#bahelite=0|!2'
#
#  Module’s channel for self-reporting.
#  The module “verbosity” itself doesn’t make this channel a group to provide
#  better processing speed, as verbosity penetrates through at least a half
#  of the library.
vchan setup '!#Verbosity=2'
#
#  ^ See also descriptions in  show_help_on_verbosity_channel_*()  above
#


 # Postload job to parse --help-verbosity-CHANNELNAME. Obviously, for the
#  function to be able to show anything, all core modules must load first.
#
bahelite_parse_--help-verbosity() {
	local  arg
	local  chname_pat='[A-Za-z_][A-Za-z0-9_-]*'
	local  ch_name
	local  help_func


	for arg in "${ARGS[@]}"; do

		[ "$arg" = '--help-verbosity' ] && {
			show_help_on_verbosity
			exit 0
		}

		[[ "$arg" =~ ^--help-verbosity=[\'\"]?($chname_pat)[\'\"]?$ ]] && {
			ch_name=${BASH_REMATCH[1]}
			help_func="show_help_on_verbosity_channel_$ch_name"
			if	   [ -v VERBOSITY_CHANNELS[$ch_name]        ]  \
				&& [ "$(type -t "$help_func")" = 'function' ]
			then
				$help_func
				exit 0
			fi

			(( ${VERBOSITY_CHANNELS[Verbosity]} >= 3 ))  \
				&& warn "Bahelite: --help-verbosity: unknown channel “$ch_name”.
				         Leaving it to be parsed by the main script."
		}
	done

	return 0
}

BAHELITE_POSTLOAD_JOBS+=( bahelite_parse_--help-verbosity )



return 0



 # NOTES
#
#  Considerations on “bahelite” verbosity channel:
#    - turn on lvl 1 when any of the bootstrap/core modules turn
#      their channel on?
#    - a prefix “Bahelite” must be added to messages on the loading stage,
#      when no Bahelite channels are verbose. (So that it would be clear,
#      that an error or a warning comes from the library adn not from the
#      main script.) But when verbosity is turned on, this prefix may be
#      removed from messages, if instead Bahelite would report when it
#      starts and ends to console. Level 1 may come useful for that.
#