#  Should be sourced.

#  error_handling.sh
#  Places traps on signals ERR, EXIT group and DEBUG. Catches even those
#    errors, that do not trigger sigerr (errexit is imperfect and doesn’t
#    catch nounset and arithmetic errors).
#  On error prints call trace, the failed command and its return code,
#    all highlighted distinctively.
#  Each trap calls user subroutine, if it’s defined (subroutine should
#    have the same name sans the “bahelite_” prefix).
#  © deterenkelt 2018–2020


# Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_ERROR_HANDLING_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_ERROR_HANDLING_VER='1.8.1'


 # Self report verbosity channel
#  bahelite_on_exit /must not/ be included: and if it would appear as a chan-
#  nel itself, then when it would call “vchan list”, the list wouldn’t be
#  printed on Verbosity level 3, because on top of that Error_handling level 3
#  would be required.
#
vchan setup '!#Error_handling:bahelite_on_error=2'


show_help_on_verbosity_channel_Error_handling() {
	cat <<-EOF
	Error_handling (self-report channel)
	    Controls the persistence of TMPDIR ($MYNAME’s directory
	    for temporary files).
	    Levels:
	    3 – no additional messages (the default).
	    4 – verbose output in regard to subshells, user hooks, (the lack of)
	        logging and cyclic entrance to same functions.
	EOF
}


__show_usage_error_handling_module() {
	cat <<-EOF
	Bahelite module “error_handling” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_error_handling_module
		exit 0

	else
		redmsg "Bahelite: error_handling: unrecognised option “$opt”."
		__show_usage_error_handling_module
		exit 4
	fi
done
unset opt



 # Stores values, that environment variable $LINENO takes, in an array.
#
#  There are three types of errors in bash.
#  1. The ones that trigger SIGERR and are caught simply by placing
#     a trap on that signal (implying that you use “set -E”).
#     To stop the script from executing, errexit is enough (set -e);
#  2. Errors that DON’T trigger SIGERR and the corresponding trap,
#     but make the script stop. If you wanted to print a call stack or do
#     something else – this all would be bypassed. Errors of this type
#     include “unbound variable”, caused when nounset option is active
#     (set -u). The script is stopped automatically, but the trap on EXIT
#     must catch the bad return value and call trap on ERR to print
#     call stack.
#  3. Error that neither trigger SIGERR, nor do they stop the script.
#     These are the most nasty ones: you get an error, but the code continues
#     to run! No way to catch, if they happened. Arithmetic errors, such as
#     division by (a variable, that has a) zero is one example of this type.
#  3.a (should actually be №3, and the current №3 should go by №4) subshell
#     in a string. The thing is, that if a variable is assigned the result
#     of a subshell, the assignment as an operation *bears the return code
#     of the subshell return code*. I.e.
#         var=$(command1 arg1 arg2)
#     is possible to catch. (Not sure if the trap on DEBUG is necessary to
#     catch it, but Bahelite catches this type of errors just fine.)
#     However, when a subshell is placed within a string, the return code
#     is overlapped by the *string assignment* operation. I.e.
#         var="some string $(command1 arg1 arg2) string continues"
#     will not stop the script. Though the SIGERR is triggered, and if set -eE
#     is used, then the trap will be triggered within the subshell. The error
#     will remain only in the scope of subshell, so if you assign a string
#     in a cycle, like that
#         for i in "${soma_array[@]}"; do
#             var="some string $(command1 arg1 arg2) string continues"
#             …
#             …
#         done
#     then you will see a bunch of errors, but the script won’t stop.
#
#     One way to avoid this is to not put subshell calls in quotes. Ever.
#     It places a huge handicap on their usefulness.
#
#     The other way is to somehow leave a mark for the main shell, when SIGERR
#     is encountered within a subshell. But this means, that the trap on DEBUG
#     must do a check for that mark *after every command* which will probably
#     slow down things a lot. For this reason simply putting a file in TMPDIR
#     is a bad option (though a working one). A better way would be to change
#     some internal variable in the main shell, like the current working direc-
#     tory or create a specifically named background job. But changing the
#     directory stack or PWD in the subshell affects only the child process,
#     and creating a background job is problematic too. Simply forking
#     a sleep with 99999 seconds doesn’t work, because sleep still runs within
#     a subshell and the main shell wait for the entire group to finish. (Sleep
#     is forked! Disowning it will probably detach it from the group list,
#     so it couldn’t be checked easy with “jobs” in the trap on DEBUG, disown-
#     ing it makes no sense in spawning it. If we’ll have to use a costly call
#     to pgrep on each command, this isn’t much better, than testing a file
#     in TMPDIR for existence.) Ideally, the job should spawn in a sleeping
#     state, like if C-z was sent to it, and the process should have a clear
#     name like “: encountered an error and must quit” (the colon is an opera-
#     tor) – so that the job could be easily found by “%:\ encountered an error”
#     job specificator.
#
#  The only way to catch the errors of type 2 is to pass the exit code
#     of the last command to the trap on EXIT, and if it’s >0, call
#     the trap on ERR, however, both BASH_LINENO and LINENO will be
#     useless in printing the right call stack:
#     - BASH_LINENO will always have “1” for the line, that actually triggered
#       the error, or at best may point to the function definition (not to the
#       line in it), where the error occurred.
#     - LINENO just cannot be used, because it stores only one value –
#       the number of the current line, which, if referenced inside
#       of a trap, would point to the command inside the trap, or,
#       if passed to a trap as an argument, would actually pass the
#       line number, where that trap is defined. Basically useless here.
#
#  This array is used to store $LINENO values, so that the trap on EXIT
#    could get the actual line number, where the error happened, and pass it
#    to the trap on ERR. Having it and knowing, that it’s called from the
#    trap on exit, trap on ERR can now substitute the wrong line numbers
#    from BASH_LINENO with the number passed from the trap on EXIT and print
#    the call stack the right way independently of whether a bash error
#    triggered SIGERR or not.
#
#  It works in the global (i.e. “main” function’s) scope, as well as inside
#    functions, sourced scripts and functions inside them.
#
#  Sharp ones among you may wonder “What about errors of the third type?”
#    The answer is: it’s not possible to catch them. You have to know,
#    what triggers them and use constructions, that prevent these errors
#    from appearing, so that there won’t be a single chance of main script
#    failing there.
#  Catching the errors of type 2 already requires a trap on DEBUG signal.
#    I’ve made a prototype of this module, that uses this trap to also check
#    the return value of the last executed command. Much like the trap
#    on EXIT does, but “one step before”. It was possible to catch a line like
#        $((  1/0  ))
#    but the trap on DEBUG could not be used. Yes, because it doesn’t
#    differentiate between simple commands and those running in “for”, “while”
#    and “until” cycles; it catches first command in an && or || statement,
#    catches the first command in a pipe without “pipefail” option set.
#    In other words, it would require another array for storing BASH_COMMAND
#    values, looking there for cycles, pipes and logical operators – and there
#    still won’t be a guarantee, that it will be done right.
#  Thus, the only way to avoid type 3 errors is to know them and use
#    constructions in your code, that exclude any possibility
#    of these errors happening.
#  P.S. this trap on debug is also useless for catching forgotten backslashes
#    in compound logic statements like
#        if  (
#              command1 \
#              && command2 \
#              && command3
#            )  # <-- forgotten backslash
#            ||
#            foovar=bar
#        then
#            …
#        fi
#
declare -gax BAHELITE_STORED_LNOS=()


 # Helps to build a meaningful stack trace, when an error is encountered.
#    Due to the different nature of bash errors – see the huge comment above –
#    the regular means are not sufficient.
#  Requires trap on debug to be set and functions, subshells and command sub-
#    stitutions to inherit it, i.e.  bahelite_toggle_ondebug_trap  must be
#    called below to set DEBUG trap and the mother script should have “set -T”.
#
bahelite_on_each_command() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	local  i
	local  line_number="$1"
	#  We don’t need more than two stored LINENO’s:
	#  - one for the failed command we want to catch,
	#  - and one that inevitably caused by calling this trap.
	local lnos_limit=2

	for ((i=${#BAHELITE_STORED_LNOS[@]}; i>0; i--)); do
		BAHELITE_STORED_LNOS[i]=${BAHELITE_STORED_LNOS[i-1]}
	done
	(( ${#BAHELITE_STORED_LNOS[@]} == (lnos_limit+1) ))  \
		&& unset BAHELITE_STORED_LNOS[$lnos_limit]
	BAHELITE_STORED_LNOS[0]=$line_number

	 # Call user’s on_debug(), if defined.
	#  Causes pipes like “echo | cat” to hang (still in bash 4.4).
	#
	#  To check if this affects you, add “set -T” before the command (assuming
	#    that the trap on DEBUG is already set). If it can cause your script
	#    hang, this problem is affecting you.
	#        set -T
	#        echo "something" | cat
	#  It also can make the main script miss signals like SIGINT and even
	#    SIGQUIT, if you do a call like exec in start_logging()
	#    in bahelite_logging.sh, without temporarily switching off functrace
	#    and disabling the trap on DEBUG. However, this occurs only under
	#    uncertain circumstances and may depend on the host. A thorough compa-
	#    rison between two hosts, one on which the issue is observed and the
	#    other, where it isn’t, has shown NO DIFFERENCES between:
	#    - shell options in $- variable
	#    - shell options in set -o
	#    - shell options in shopt -p
	#    - TERM variable
	#    - stty output
	#    - I/O file descriptors, used in the main shell and subshell (except
	#      for the pipe:XXXXXXXXX numbers)
	#    - traps being set in the main script and within >( … )
	#      (Running “trap '' DEBUG RETURN” within >( … ) also didn’t help.)
	#    The comparison was indeed performed without temporary disabling
	#    functrace and unsetting the trap on DEBUG.
	#  So, if you decide to uncommend the line below to have the on_debug()
	#    function from the main script being called from here, make sure
	#    to temporarily turn off the trap on DEBUG and the functrace shell
	#    option whenever you echo something to a pipe, or where process
	#    substitution is involved, otherwise you’ll get reports, that
	#    your script hangs.
	#        functrace_off
	#        <your code>
	#        functrace_on
	#    This will control both the functrace shell option and set/unset
	#    the trap on DEBUG, as needed.
	#  Mind, that some tools may cause additional troubles, e.g. xclip may
	#    go into waiting mode without -verbose, until you close stdout or
	#    redirect it. See https://github.com/asweigart/pyperclip/issues/116
	#  List of things that do not help:
	#    - set +T
	#    - builtin set +T
	#    - bahelite_functrace_off
	#    - “$(trap '' RETURN; type -t on_debug)”
	#
	# [ "$(type -t on_debug)" = 'function' ] && on_debug

	 # Output to stdout during DEBUG trap may produce unwanted output
	#    into $(subshell calls), so you better NEVER output anything
	#    in traps on DEBUG, or at least always use >&2 and make sure
	#    you never add stderr to stdout in $(subshell calls) like $(… 2>&1).
	#  Xdialog has --stdout option to produce output in stdout instead
	#    of stderr.
	# echo "${BAHELITE_STORED_LNOS[*]}" >&2

	return 0
}
export -f  bahelite_on_each_command


 # Trap on DEBUG is a part of the system, that helps to build a meaningful
#    call trace in case when an error happens.
#  Trap on DEBUG is temporarily disabled for the time xtrace shell option
#    is enabled in the mother script. This is handled in the set builtin
#    wrapper in bahelite.sh.
#
bahelite_toggle_ondebug_trap() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	case "$1" in
		set)
			#  Note the single quotes – to prevent early expansion
			trap 'bahelite_on_each_command "$LINENO"' DEBUG
			;;
		unset)
			#  trap '' DEBUG will ignore the signal.
			#  trap - DEBUG will reset command to 'bahelite_on_each_command… '.
			trap '' DEBUG
			;;
	esac
	return 0
}
export -f  bahelite_on_each_command


 # This trap handles various signals that cause bash to quit.
#  SIGEXIT is handled, because not all errors trigger SIGERR. Because of that,
#    when the program wants to quit “normally” on EXIT, we should check the
#    data collected in bahelite_on_each_command() – the trap on DEBUG – first,
#    to know whether there was some failed command or instruction, and if
#    there is, throw an error message to desktop and print the call stack
#    to the log. If we won’t do it, then on the user side this will seem like
#    a silent failure. Nasty.
#  SIGQUIT is always ignored by bash. On catching SIGQUIT bash only prints
#    “Quit”, “Segmentation fault” and no trap is processed.
#  SIGKILL forcibly wipes the process from the memory, this doesn’t leave any
#    possibility to handle this case. Just as planned by the OS, actually.
#  SIGINT, SIGTERM, SIGHUP and SIGPIPE all are common causes of forced termi-
#    nation, and these four signals are successfully handled with bash traps.
#
bahelite_on_exit() {
	builtin set +x
	declare -g __bahelite_onexit_entrance_counter
	let '++__bahelite_onexit_entrance_counter,  1'

	local  in_subshell

	(( BASH_SUBSHELL > 0 ))  \
		&& in_subshell="while in ${__bri}subshell${__s}"  \
		|| in_subshell="while in the ${__bri}main shell${__s}"

	info-4 "Entering bahelite_${__bri}on_exit${__s} $in_subshell for the ${__bri}$__bahelite_onerr_entrance_counter time${__s}."

	trap ''  DEBUG  EXIT  TERM  INT  HUP  PIPE  ERR  RETURN

	local  command="$1"
	local  retval="$2"
	local  stored_lnos="$3"
	local  signal="$4"
	local  current_varlist
	local  new_variables
	local  variables_not_to_print
	local  verbosity_depth=${VERBOSITY_CHANNELS[Error_handling]}

	mildrop

	 # Normally, when a subshell exits, trap on EXIT is called in the main
	#  shell. However, if called explicitly, e.g. from another trap – on ERR
	#  for example (Bahelite doesn’t do that) it may be called in the sub-
	#  shell context. Nesting subshells, as a rule, do not make the trap
	#  on EXIT run in subshell context too. So this check here is just
	#  in case.
	#
	(( BASH_SUBSHELL > 0 ))  && exit $retval

	 # Like with BAHELITE_ERROR_PROCESSED and bahelite_show_error, there is
	#  a situation, when the same trap may be called twice, though it is
	#  unnecessary. The reason here is that after SIGINT bash calls SIGEXIT,
	#  and as both signals are processed by this same function, there is
	#  no need to process what already was processed.
	#
	[ -v BAHELITE_EXIT_PROCESSED ]  \
		&& exit $retval  \
		|| declare -gx BAHELITE_EXIT_PROCESSED=t

	 # Catching internal bash errors, like “unbound variable”
	#    when using nounset option (set -u). These errors
	#    do not trigger SIGERR, so they must be caught on exit.
	#  If the main script runs in the background, the user
	#    won’t even see an error without this.
	#  “exit” is the only command that allowed to quit with non-zero safely.
	#    It implies, that the error was handled.
	#  There are cases, when one has to catch a bad exit code from a subshell,
	#    like with
	#      $( … ) || exit $?
	#    But the “exit” directive is tricky here. Normally, one uses “exit” in
	#    two cases: when exit status is clean, i.e. equals 0, and when an error
	#    is already shown, e.g. when err() was called inside the subshell – it
	#    has already displayed an error to the user, it just couldn’t stop the
	#    main script – so we run exit afterwards in “… || exit $?”. But the
	#    subshell code may still catch a bash error. And since the subshell is
	#    a part of an OR statement (i.e. “…||…”), the proper signal – SIGERR – 
	#    won’t be triggered. The error will make the script stop, but it will
	#    bypass the trap on SIGERR, and this means, that neither in the con-
	#    sole, nor in the log will be a mention WHY the script stopped – this
	#    would look like it just stopped normally. This is called a “silent
	#    failure”.
	#  To prevent that, the condition below does a check on the actual number
	#    in the return code – if it’s 1 or 2, that would most likely be an
	#    uncaught bash error. And they will go by the “else” clause here,
	#    to bahelite_show_error().
	#
	[ "$signal" = 'EXIT' ]  &&  (( retval > 0  &&  retval != 6 ))  &&  {
		if [[ "$command" =~ ^exit[[:space:]] ]]  &&  ((retval >= 5)); then

			[ -v BAHELITE_FORCE_CALL_STACK ] || [ ! -v BAHELITE_STIPULATED_ERROR ]  \
				&& print_call_stack

			if	   [ -v BAHELITE_STIPULATED_ERROR ]  \
				|| [ -r "${TMPDIR:-/tmp}/BAHELITE_STIPULATED_ERROR_IN_SUBSHELL" ]
			then
				[ "$(type -t on_error)" = 'function' ] && on_error
			fi
		else
			[ ! -v BAHELITE_ERROR_PROCESSED ]    \
				&& bahelite_on_error "$command"   \
				                     "$retval"     \
				                     "from_on_exit" \
				                     "${stored_lnos%% *}"
				                     # "${stored_lnos##* }"
			#  ^ user’s on_error() will be launched from within
			#    bahelite_on_error()
		fi
	}

	#  Run user’s on_exit(). Keep in mind, that at this point
	#  $? is already frozen.
	[ "$(type -t on_exit)" = 'function' ] && on_exit

	[ -v BAHELITE_DUMP_VARIABLES ] && {
		#
		#  ↓ This list shouldn’t have variables read from (user’s) RC file.
		#
		current_varlist=$(compgen -A variable)

		#  Pattern list for the variables that shouldn’t be logged
		#
		variables_not_to_print=(
			'BAHELITE_(EXIT|ERROR)_PROCESSED'

			#  May be unset
			#
			'BAHELITE_BRING_BACK_.*'

			#  Only BAHELITE_VARLIST_AFTER_STARTUP is removed, for the
			#  output is going to get uniq’ed, and AFTER_STARTUP variable
			#  would be present only once, while BEFORE_STARTUP will be there
			#  twice and thus removed automatically by “uniq -u”.
			#
			'BAHELITE_VARLIST_AFTER_STARTUP'

			#  Any module that was loaded after Bahelite finished startup
			#  (this is common for optional modules), will declare its pre-
			#  sence after BAHELITE_VARLIST_BEFORE_STARTUP is set, thus some
			#  presence declaration variables will not be auto-nullified.
			#
			'BAHELITE_MODULE_.*_VER'

			#  Obviously we have it set if variables are going to be printed.
			#
			BAHELITE_DUMP_VARIABLES

			#  This very list too.
			#
			variables_not_to_print
		)

		new_variables=(
			$(	echo "${BAHELITE_VARLIST_AFTER_STARTUP:-}"$'\n'"$current_varlist"  \
				    | grep -vE "($(IFS='|'; echo "${variables_not_to_print[*]}"))"  \
				    | sort | uniq -u | sort
			 )
		)

		declare -p "${new_variables[@]}"  >"${LOGDIR:-${TMPDIR:-/tmp}}/variables"
	}

	[ "$signal" != 'EXIT' ] && redmsg "Caught SIG$signal."

	#  List verbosity channels, if requested.
	(( ${VERBOSITY_CHANNELS[Verbosity]} >= 3 )) && vchan list

	#  Stop logging, if started
	[ "$(type -t stop_logging)" = 'function' ] && stop_logging

	[ "$(type -t delete_tmpdir)" = 'function' ] && delete_tmpdir

	#  ULTRAKILL. Don’t do that.
	#
	# kill -- -$ORIG_BASHPID
	return 0
}
#  No export: it runs only on the top level. Using it inside of subshells is
#  not necessary (exit from subshell is caught and handled) and could cause
#  harm, if they would run twice.



 # Prints the stack of the function calls
#
#  This function should have had “bahelite_” prefix, and the user’s variant
#  would be without prefix, however, this would tremendously complicate things:
#    - another intermediate function in the calling stack would add a bunch
#      of checks to the code, which is already complex;
#    - thus a user variant would never have appeared – complicates too much;
#    - then people thinking, that they only need to create their alias to this
#      function, and they’ll have a handy stack printer func, would make
#      a grave mistake.
#  So in order to avoid this, this function was presented as a user facility,
#  thus removing the need to create aliases. This function determines, whether
#  it’s called by the user (the author of the main script) for whatever purpo-
#  ses they have from being called as a part of error handling, by checking
#  the function names in the stack, so it can act as both an inner tool and
#  a user’s facility since the version 1.7 of the “error_handling” module.
#
print_call_stack() {
	echo  # in case call stack printed before a line ends.

	 # Indicator or a call by the user (not due to an error). Lifts the
	#  restriction on printing stack only once (due to the nature of subshells
	#  it may unnecessarily be printed twice – this is confusing and Bahelite
	#  prevents that.)
	#
	local flying_call=t
	local f
	for ((f=0; f<${#FUNCNAME[*]}; f++)); do
		[[ "${FUNCNAME[f]}" =~ ^bahelite_on(exit|error)$ ]] && {
			unset flying_call
			break
		}
	done

	 # In the short period during loading modules, an error may happen
	#  between loading this module and “tmpdir”, in which case TMPDIR
	#  wouldn’t be set yet.
	#
	[ ! -v flying_call  -a  -e "${TMPDIR:-}/call_stack_printed" ] && {
		info-4 "Not printing call stack: already printed."
		return 0
	}
	#  Every output from this function must go to stderr,
	#  because *err*() may as well be called from a subshell
	#
	#  Skip only 3 levels (this very function, __msg and err*/abort), when
	#  printing the call stack.
	local  levels_to_skip
	local  from_subshell
	local  from_on_exit="${1:-}"
	local  real_line_number="${2:-}"
	local  line_number_to_print
	local  i

	if [ -v BAHELITE_STIPULATED_ERROR ]; then
		#  If we’ve come here from a function like err(),
		#  and the error is triggered and handled by us.
		#  print_call_stack ← bahelite_on(exit|error) ← __msg ← err
		levels_to_skip=3

	else
		#  If this function was called directly at some place in the code.
		#  (It may be even not an error – just the author of the main script
		#   debugging things.) Here we want to go down the stack up the caller
		#   of this function.
		levels_to_skip=0
	fi
	(( BASH_SUBSHELL > 0 )) && from_subshell=' (from subshell)'
	divider_message "Call stack${from_subshell:-}"  '-'  "$__bright" >&2

	#  Dropping message indentation level either to the level upon entrance,
	#  or completely, if the output is on top level.
	[ "$__mi" = '  ' ] && __mi=''

	for ((f=${#FUNCNAME[@]}-1; f>levels_to_skip; f--)); do
		#  Hide on_exit and on_error, as the error only bypasses through
		#  there. We don’t show THIS function in the call stack, right?
		[ "${FUNCNAME[f]}" = bahelite_on_error ] && continue
		[ "${FUNCNAME[f]}" = bahelite_on_exit ] && continue
		line_number_to_print="${BASH_LINENO[f-1]}"
		#  If the next function (that’s closer to this one) is on_exit,
		#  this means, that FUNCNAME[f] currently holds the name
		#  of the function, where the error occurred, and its
		#  line number should be replaced with the real one.
		[ "$from_on_exit" = from_on_exit ] && {
			[ "${FUNCNAME[f-2]}" = bahelite_on_exit ] && {
				line_number_to_print="$real_line_number"
			}
		}
		# echo "Printing FUNCNAME[$f], BASH_LINENO[$((f-1))], BASH_SOURCE[$f]"
		echo -en "${__mi}${__bri:-}${FUNCNAME[f]}${__s:-}, " >&2
		echo -e  "line $line_number_to_print in ${BASH_SOURCE[f]}" >&2
	done

	 # In the short period during loading modules, an error may happen
	#  between loading this module and “tmpdir”, in which case TMPDIR
	#  wouldn’t be set yet.
	#
	[ -v flying_call ]  \
		|| touch "${TMPDIR:-/tmp}/call_stack_printed"

	return 0
}
export -f  print_call_stack


bahelite_on_error() {
	#  Disabling xtrace, for even if the programmer has put set +x where
	#  needed, but the program catches an error before that all, there will be
	#  a lot of trace, that the programmer doesn’t need.
	#
	builtin set +x
	trap '' DEBUG

	declare -gx BAHELITE_DUMP_VARIABLES
	declare -gx BAHELITE_DONT_CLEAR_TMPDIR
	declare -gx BAHELITE_ERROR_PROCESSED
	declare -gx __bahelite_onerr_entrance_counter

	local  failed_command=$1
	local  failed_command_code=$2
	local  from_on_exit="${3:-}"
	local  real_line_number=${4:-}
	local  log_path_copied_to_clipboard
	local  current_varlist
	local  info_redir

	#  This is for bahelite_on_exit().
	BAHELITE_DUMP_VARIABLES=t

	[ -v BAHELITE_DONT_CLEAR_TMPDIR_ON_ERROR ]  \
		&& BAHELITE_DONT_CLEAR_TMPDIR=t

	mildrop

	#  Since an error occurred, let all output go to stderr by default.
	#  Bad idea: to put “exec 2>&1” here
	#  Run user’s on_error().
	#
	info-4 "Running user’s on_error()…"
	milinc-4

	if [ "$(type -t on_error)" = 'function' ]; then
		on_error
		info-4 "Finished."
	else
		info-4 "User hook is not set."
	fi
	mildec-4

	#  If this is a stipulated error, that happened in a subshell,
	#  and because of that, the call to err() did only make the subshell
	#  exit(), and not the main script, the must not treat it as an unhandled
	#  error.
	#

	let '++__bahelite_onerr_entrance_counter,  1'
	local  in_subshell
	if (( BASH_SUBSHELL > 0 )); then
		in_subshell="while in ${__bri}subshell${__s}"
		#  Because in a subshell stdout is grabbed into some variable.
		info_redir='>&2'
	else
		in_subshell="while in the ${__bri}main shell${__s}"
	fi
	info-4 "Entering bahelite_${__bri}on_error${__s} $in_subshell for the ${__bri}$__bahelite_onerr_entrance_counter time${__s}." ${info_redir:-}

	 # In the short period during loading modules, an error may happen
	#  between loading this module and “tmpdir”, in which case TMPDIR
	#  wouldn’t be set yet.
	#
	if	(( failed_command_code >= 5 ))  \
		&& [ -r "${TMPDIR:-/tmp}/BAHELITE_STIPULATED_ERROR_IN_SUBSHELL" ]
	then
		return 0   #  return code >0 would break  bahelite_on_exit  procedures!
	fi

	#  Dropping message indentation level either to the level upon entrance,
	#  or completely, if the output is on top level.
	#
	[ "$__mi" = '  ' ] && __mi=''

	[ -v BAHELITE_FORCE_CALL_STACK ] || [ ! -v BAHELITE_STIPULATED_ERROR ]  \
		&& print_call_stack "${from_on_exit:-}" "${real_line_number:-}"

	[ ! -v BAHELITE_STIPULATED_ERROR ] && {
		(	echo -en "${__mi}Command: "
			echo -en "${__bri:-}$failed_command${__s:-} "
			echo -en "${__r:-}${__bri:-}(exit code: $failed_command_code)${__s:-}."
		) \
			| fold -w $((TERM_COLUMNS-9-${#__mi})) -s  \
			| sed -r "1 !s/^/${__mi}         /g"  >&2
		echo >&2
	}

	#  SIGERR is triggered, when the last executed command has $? ≠ 0.
	#  However, bash will also issue SIGEXIT afterwards (as it always does
	#  except for after SIGQUIT, as that belongs to the group of “core dump”
	#  signals, and this is probably the reason why pressing C-\ in the
	#  terminal ends with a “Segmentation fault” message and SIGEXIT is never
	#  issued). And as the SIGEXIT trap may call bahelite_on_error – because
	#  it catches even those errors, that do not trigger a SIGERR – we need
	#  to avoid calling bahelite_on_error recursively. For that we set
	#  BAHELITE_ERROR_PROCESSED to indicate, that there is no need to call
	#  this function twice.
	#
	BAHELITE_ERROR_PROCESSED=t

	#  If in a subshell, do not go to print the path to a logfile –
	#  the call to this function from the main shell will do it.
	#
	(( BASH_SUBSHELL > 0 )) && return 0

	[ ! -v BAHELITE_STIPULATED_ERROR ] && {
		if [ -v BAHELITE_LOGGING_STARTED ]; then
			which xclip &>/dev/null && {
				[ -v DISPLAY ]  \
					&& { echo -n "$LOGPATH" | xclip  >&- ; }
					#  Bloody xclip hangs without it ^^^
					#  You can remove it and see for yourself (add “-verbose”).
				log_path_copied_to_clipboard='\n\n(Path to the log file is copied to clipboard.)'
			}
			[ "$(type -t bahelite_notify_send)" = 'function' ]  \
				&& bahelite_notify_send "Bash error. See the log.${log_path_copied_to_clipboard:-}"   \
				                         error
			print_logpath >&2

		else
			if	   [ "$(type -t bahelite_notify_send)" = 'function' ]  \
				&& (( ${VERBOSITY_CHANNELS[desktop]:- -1} >= 1 ))
			then
				bahelite_notify_send "Bash error. See console." error
			fi
			(( ${VERBOSITY_CHANNELS[Error_handling]} >= 4 )) && {
				warn "Logging wasn’t enabled in $MYNAME.
				      To enable logs, add the module “logging” to BAHELITE_LOAD_MODULES."
			}
		fi
	}

	return 0
}
#  No export: must not be used in subshell context (for the same reason as
#  bahelite_on_exit() function above).


 # When it is needed to disable the errexit shell option (you usually do this
#    with “set +e”), the SIGERR is still triggered – it only doesn’t make the
#    rogram quit any more. And the trap associated with SIGERR, keeps running.
#    The programmer of the mother script doesn’t expect this, and thus
#    it creates a problem.
#  The trap on SIGERR is useful, when errexit is on: when an error happens,
#    the trap prints the details and does necessary stuff. When errexit
#    is unset, this trap becomes more of an inconvenience, rather than
#    adding usefulness. As the program will not stop, it may trigger multiple
#    SIGERR signals, which may be very confusing.
#  If the trap on SIGERR would run multiple times (because errexit is unset),
#    the prgrammer might think, that the library went insane. This is another
#    confusion, that must be prevented.
#  The state of errtrace shell option (set -E), that is often enabled together
#    with “set -e”, is not touched in any way, because this is not necessary.
#    Shell functions, subshells and substitutions will continue to inherit
#    whatever is associated with the signal, and we here change that very
#    association. Inheritance may be left as is – it doesn’t create any poten-
#    tial issue.
#
bahelite_toggle_onerror_trap() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	case "$1" in
		set)
			#  Note the single quotes – to prevent early expansion
			trap 'bahelite_on_error "$BASH_COMMAND" "$?"' ERR
			;;
		unset)
			#  trap '' ERR will ignore the signal.
			#  trap - ERR will reset command to 'bahelite_show_error… '.
			trap '' ERR
			;;
	esac
	return 0
}
#  No export: must not be used in subshell context.



__bahelite_onexit_entrance_counter=0
__bahelite_onerr_entrance_counter=0

trap 'bahelite_on_exit "$BASH_COMMAND" "$?" "${BAHELITE_STORED_LNOS[*]}" EXIT' EXIT
trap 'bahelite_on_exit "$BASH_COMMAND" "$?" "${BAHELITE_STORED_LNOS[*]}" TERM' TERM
trap 'bahelite_on_exit "$BASH_COMMAND" "$?" "${BAHELITE_STORED_LNOS[*]}"  INT'  INT
trap 'bahelite_on_exit "$BASH_COMMAND" "$?" "${BAHELITE_STORED_LNOS[*]}"  HUP'  HUP
trap 'bahelite_on_exit "$BASH_COMMAND" "$?" "${BAHELITE_STORED_LNOS[*]}" PIPE' PIPE
#
#  Traps on ERR and DEBUG signals may need to be toggled,
#  hence activated through their own wrappers.
#
bahelite_toggle_onerror_trap  set
#
#  The trap on DEBUG has a meaning to be set only when functrace shell option
#    is activated in the main script (usually with “set -T”), so on top of
#    the wrapper there is another check.
#  Trap on DEBUG is necessary for the better error handling. See also
#    the descriptions above to these functions:
#    - bahelite_on_each_command;
#    - bahelite_toggle_ondebug_trap;
#    - bahelite_on_exit.
#
[ -o functrace ]  \
	&& bahelite_toggle_ondebug_trap  set


return 0