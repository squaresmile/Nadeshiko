#  Should be sourced.

#  messages.sh
#  Provides messages for console and desktop (if messages_to_desktop module
#  is included too).
#  © deterenkelt 2018–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_MESSAGES_VER ] && return 0
bahelite_load_module 'error_codes' || exit $?
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_MESSAGES_VER='2.10.1'

#  No self-report verbosity channel: that would be more harmful (to clarity)
#  than actually useful.


__show_usage_messages_module() {
	cat <<-EOF
	Bahelite module “messages” options:

	autofold=${__bri}0…1${__s}
	    Whether to automatically fold messages longer than the current
	    terminal width. The order of preference for the tool is the following:
	      1) busybox fold (best available);
	      2) anything found in PATH that is not GNU fold (e.g. BSD fold);
	      3) GNU fold. The last, because it sees any multibyte (i.e. non-ASCII)
	         character as two, and this results in GNU fold using only a half
	         of terminal width.
	EOF

	# kw-dir=<path>
	#     The directory which holds localisation files. Path may be an absolute
	#     path or relative to MYDIR (the directory in which the main script
	#     resides).

	#     See the wiki for how to implement localisation.
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_messages_module
		exit 0

	elif [ "$opt" = autofold ]; then
		declare -gx MSG_AUTOFOLD_MESSAGES=t
		#
		#  The order of preference:
		#  1. busybox fold;
		#  2. anything except coreutils fold (e.g. BSD fold);
		#  3. coreutils fold – the last, because it shortens the line twice,
		#     for strings that consist of multibyte characters (i.e. if your
		#     language is not something ASCII, “fold” from coreutils will
		#     think, that your terminal window is twice as short).
		#
		if busybox fold --help &>/dev/null; then
			declare -gx __fold="busybox fold"

		elif [ "$(type -t fold)" = file ]; then
			declare -gx __fold="fold"
			[[ "$(fold --version | head -n1)" =~ ^fold\ \(GNU\ coreutils\) ]]  && {
				warn 'GNU fold doesn’t support multibyte characters, hence all strings
				      made of non-ASCII characters will be folded twice as short.
				      To avoid this issue, install busybox with “fold” applet. Alterna-
				      tively, install BSD fold and make sure that its path in the PATH
				      variable precedes the path, where coreutils fold resides.'
			}
			#  BSD fold, for example would have here
			#  “usage: fold [-bs] [-w width] [file ...]”
		fi

	else
		redmsg "Bahelite: messages: unrecognised option “$opt”."
		__show_usage_messages_module
		exit 4
	fi
done
unset opt



                         #  Message lists  #

 # keyworded message lists
#
declare -gAx BAHELITE_INFO_MESSAGES=()
declare -gAx BAHELITE_WARNING_MESSAGES=()

 # Error messages
#  Keys are used as parameters to err() and values are printed via msg().
#  The keys can contain spaces – e.g. ‘my key’. Passing them to err() doesn’t
#    require quoting and the number of spaces is not important.
#  You can localise messages by redefining this array in some file
#    and sourcing it.
#
declare -gAx BAHELITE_ERROR_MESSAGES=(
	[no such msg]='No such message keyword: “$1”.'
)

 # User message lists
#  By default, functions like info(), warn(), err() will accept a text string
#  and display it. However, it’s possible to replace strings with keywords
#  and hold them separately. This comes handy, when
#  - the messages are too big and ruin the length of lines in the code;
#  - especially when you’d like to use the text of the message as a template,
#    and pass parameters to err(), so that it would substitute them – making
#    a big string with big variable names inside may be really ugly.
#  - when you want to localise your script and keep language-agnostic keywords
#    in the code while pulling the actual messages from a file with localisa-
#    tion.
#  In order to enable keyword-based messages, define MSG_USE_KEYWORDS with
#  any value in the main script. This will switch off the messaging system
#  to arrays.
#
# declare -x MSG_USE_KEYWORDS=t
#
declare -gAx INFO_MESSAGES=()
declare -gAx WARNING_MESSAGES=()
declare -gAx ERROR_MESSAGES=()


 # Colours for the console and log messages
#  Regular functions (info, warn, err) apply it only to asterisk.
#
#  Somebody may have an idea to use these variables to colour their own
#  output, but if NO_COLOURS would be set, such usage may end with a bash
#  error, so there should be at least an empty value.
#
declare -gx INFO_MESSAGE_COLOUR=${__green:-}
declare -gx WARN_MESSAGE_COLOUR=${__yellow:-}
declare -gx ERR_MESSAGE_COLOUR=${__red:-}
declare -gx PLAIN_MESSAGE_COLOUR=${__stop:-}
declare -gx HEADER_MESSAGE_COLOUR=${__yellow:-}${__bright:-}
declare -gx DEBUG_MESSAGE_COLOUR=${__magenta:-}


 # Define this variable to start each message not with just an asterisk
#    ex:  * Stage 01 completed.
#  but with a keyword that would define the type of the message. Especially
#  handy if you load “bahelite” module with “no-colours” option.
#    ex:  * INFO: Stage 01 completed.
#
# declare -gx MSG_ASTERISK_WITH_MSGTYPE=t


 # Define this variable to make verbose messages (of 4+ level) have also
#  the verbosity channel name before the text of the message.
#
#declare -gx MSG_VERBOSE_MSG_WITH_CHNAME=t



               #  Messages to console, log and desktop  #

 # Message properties array
#
#  message_array
#      When message functions are passed keywords instead of actual message
#      text, this property holds the name of the associative array, where
#      the pairs of keyword – message text are stored.
#
#  colour
#      Defines the “colour code” for the message. This property holds the
#      name of the global variable that defines the colour for a family of
#      functions, e.g. INFO_MESSAGE_COLOUR. The colour will be used only for
#      the asterisk, that begins the message. If MSG_DISABLE_COLOURS is set,
#      then it has no effect.
#
#  whole_message_in_colour
#      Whether to colour the whole message in colour instead of just the
#      asterisk. Values are “yes” or “no”.
#
#  asterisk
#      The string to prepend the actual message with, typically an asterisk
#      with a space. If MSG_DISABLE_COLOURS is set, then the asterisk colour
#      cannot convey the message type and thus it is added explicitly, e.g.
#      “ERROR:” or “WARNING:”.
#
#  desktop_message
#      Whether the message should be duplicated as a desktop message (i.e.
#      sent with notify-send). Values are “yes” or “no”.
#
#  desktop_message_type
#      The type of message to pass for “notify-send”. If “desktop_message”
#      is set to “yes”, then this property should be set to either “info”,
#      or “dialog-warning” or “dialog-error”. If “desktop_message” is set
#      to “no”, then this property may hold an empty string.
#
#  stay_on_line
#      If set to “yes”, then newline isn’t added at the end of the message,
#      allowing the next command (typically an “echo”), to continue the line.
#      It is basically the “-n” switch to echo. For most messages should be
#      set to “no”. Messages duplicated to desktop ignore this option – 
#      for them the ending newline is always cut off.
#
#  output
#      Destination output for console messages: “stdout” or “stderr”.
#      Must be always set.
#
#  keyworded
#      Whether the message is keyworded, i.e. the message function is passed
#      a keyword rather than an actual message text. Values are “yes” or “no”.
#      Allows the function to use keyworded functionality without setting the
#      global variable MSG_USE_KEYWORDS. Internal functions such as err-ikw()
#      use this.
#
#  exit_code
#      Whether the message function should initiate an exit from the program.
#      Only *err*() and abort() use exit codes. For them this property holds
#      a number that they should pass to the “exit” builtin. All the other
#      message functions must have an empty string here.
#
#      On exit codes
#
#      1 – is not used. It is a generic code, with which the main script may
#          exit, if the programmer forgets to place his exit’s and return’s
#          properly. Let these mistakes be exposed.
#      2 – is not used. Bash exits with this code, when it catches an inter-
#          preter or a syntax error. Such errors may happen in the main script.
#      3 – Bahelite exits with this code, if the system runs an incompatible
#          version of the Bash interpreter.
#      4 – Bahelite uses this code for all internal errors, i.e. related to
#          the inner mechanics of this library. Most commonly an exit code 4
#          means a failed self-check and the initialisation stage: a lack in
#          minimal dependencies, failing to load a module, on an unsolicited
#          attempt to source the main script (instead of executing it).
#      5 – any error happening in the main script after Bahelite is loaded.
#          To use custom error codes, use MSG_USE_KEYWORDS and the ERROR_CODES
#          array (see above).
#      6 – an exit code issued by the abort() function. This function is to be
#          used instead of err() in cases, when the end user of the main script
#          intentionally interrupts the execution at some stage (e.g. presses
#          “Cancel”). The abort() messages have appearance of informational
#          ones, but since the program didn’t complete whatever it was made
#          for, the exit code “0” doesn’t suit here. The exit code “5” doesn’t
#          suit either, because the exit happens not because of a user mistake
#          or due to an uncontrolled situation that has lead to a fault.
#      7…125 – free for the main script. Can be used with ERROR_CODES.
#      126…165 – not used by Bahelite and must not be used in the main script:
#          this range belongs to the interpreter.
#      166…254 – free for the main script. Can be used with ERROR_CODES.
#      255 – is not used. This is an ambiguous code, that Bash triggers for
#          more than one reason.
#
#      You are strongly advised to rely on err() and abort() to exit from the
#      program:
#        - they make the exit builtin work from subshells;
#        - like all message functions, they print their messages directly
#          to console/log, preventing them to be caught by subshell, i.e. $();
#        - err() prints a nice call stack;
#        - err() and abort() by default duplicate their messages to desktop
#          (if the module is loaded);
#        - err() has a companion redmsg(), which you may use to print
#          a detailed error message to console before calling err, whose
#          message would contain a concise reason.
#
#      There’s no need to employ ERROR_CODES and MSG_USE_KEYWORDS, unless you
#      absolutely have to have distinctive error codes.
#
#      If you have to use custom exit codes (be it for ERROR_CODES or for
#      a custom err()-like wrapper), use only ranges 7…125 and 166…254.
#
#  verbosity_minlevel
#      Determines, at which minimum verbosity level this function is allowed
#      to display the message passed to it. Must be a number in range 0…99999.
#
#      Channels that control messages are silent at level 0, allow only error-
#      level messages at level 1, errors and warnings at level 2 and on level 3
#      they display also informational messages. Level 3 thus is the default
#      level for the most channels, and the verbose/debugging-level messages
#      are hidden unless the level is switched to 4 or above.
#
#      Channels that control *outputs* – and there’s only a few of them:
#      “console”, “log”, “desktop” and “xtrace” – restrict messages on a
#      higher level than this property “verbosity_minlevel”, so don’t look
#      at the levels of those channels: their levels is their own business,
#      specific to their particular output. And despite that the “desktop”
#      channel looks like a message channel, it is only made to look so
#      (the difference is vague now, though).
#


#  For arguments, see __msg() below.


 # Shows an informational message
#
info() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* "
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  info


 # Same as info(), but omits the ending newline, like “echo -n” does.
#
infon() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* "
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='yes'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  infon


 # Shows an info message and then waits for the specified command to finish,
#  depending on the exit code, continues the message with either [ OK ]
#  or [ Fail ]. In case the exit code was non-zero, prints the output of the
#  command.
#
#  $1 – a message. Something like “Starting up servicename… ”
#  $2 – a shell command.
#  $3 – any string to force the output even if the exit code was 0. This is
#       handy for poorly written programs that return 0 even on error.
#
info-wait() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	local  message=$1
	local  command=$2
	local  force_output="$3"
	local  outp
	local  result

	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+RUNNING: }"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='yes'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$message"

	outp=$( bash -c "$command" 2>&1 )
	result=$?
	(( result == 0 )) \
		&& echo -e "${__bri:-} [ ${__g:-}OK${__s:-}${__bri:-} ] ${__s:-}"  \
		|| echo -e "${__bri:-} [ ${__r:-}Fail${__s:-}${__bri:-} ]${__s:-}"
	if [ "$outp" ]; then
		[ $result -ne 0  -o  "$force_output" ] && {
			milinc
			info "Here is the output of ‘$command’:"
			plainmsg "$outp"
			mildec
		}
	else
		plainmsg "(No output from “$command”.)"
	fi
	return 0
}
export -f  info-wait


 # Prints a warning message to console.
#
warn() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warn


 # Shows an error message and calls the “exit” builtin.
#  If the “messages_to_dekstop” module is loaded, also duplicates the message
#  to desktop, so it’s good to keep the message concise. For a big descriptive
#  message better use redmsg() before calling err(). Prints call stack. Dumps
#  the variable list to LOGDIR, if logging is enabled. The exit code is 5,
#  unless you employ MSG_USE_KEYWORDS and the ERROR_CODES array.
#
err() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]='5'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#  ^ Exits.
}
export -f  err


 # Displays a message on console with an appearance of err(), but doesn’t do
#  anything beyond that.
#
redmsg() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	return 0
}
export -f  redmsg


err-stack() {
	declare -gx BAHELITE_FORCE_CALL_STACK=t
	err "$@"
	#  ^ Exits.
}
export -f  err-stack


 # An info message marking a dead end
#  It suites to a situation, when the main script attempts to go some auxili-
#  ary route, but that attempt wasn’t successful. The execution then returns
#  to a point before and continues from there. Message indentation should
#  probably return several levels back at the same time too. This is similar
#  to a situation when in real life you’re driving, then encounter a roadblock
#  and go around.
#
denied() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="× ${MSG_ASTERISK_WITH_MSGTYPE:+DENIED: }"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  denied


 # Shows a hint for the previous message
#  Shell scripts often run external programs, and those sometimes have weird,
#  ambiguous or confusing messages. In case there is no way but to allow such
#  message to be shown, it is good to output a hint how to understand the
#  previous message, to tell whether it’s worrisome or may be ignored.
#
sub-msg() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='PLAIN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="^ "
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  sub-msg


 # Same as err(), but prints the whole line in red.
#  More of an example for a whole-coloured message, than something that
#  should really be used. Has no advantages over err(). except for being
#  annoyingly red.
#
errw() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='yes'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='no'
		[exit_code]='5'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#  ^ Exits.
}
export -f errw


 # Like err(), but has the appearance of an info message. For the case when
#  the end user chooses to halt the execution and finish the program halfway. 
#
#  The message should be concise, as it’s duplicated to desktop. Usually
#  something like “Cancelled.” is enough.
#
#  abort() is to be used when the main script *offers the user an option* –
#  a menu item or a button – to end the program preventively. Ctrl-C or kill-
#  ing the process are forceful interruptions not related here.
#
abort() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='INFO_MESSAGES'
		[colour]='INFO_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ABORT: }"
		[desktop_message]='yes'
		[desktop_message_type]='info'
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]='6'
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	#  ^ Exits.
}
export -f  abort


 # Bahelite keyworded info() for internal use
#
#info-ikw () {
#
#}


 # Bahelite keyworded warn() for internal use
#
warn-ikw() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='BAHELITE_WARNING_MESSAGES'
		[colour]='WARN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+WARNING: }"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='yes'
		[exit_code]=''
		[verbosity_minlevel]='2'
	)
	__msg "$@"
	return 0
}
export -f  warn-ikw


 # Bahelite keyworded err() for internal use
#
err-ikw() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='BAHELITE_ERROR_MESSAGES'
		[colour]='ERR_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="* ${MSG_ASTERISK_WITH_MSGTYPE:+ERROR: }"
		[desktop_message]='yes'
		[desktop_message_type]='err'
		[stay_on_line]='no'
		[output]='stderr'
		[keyworded]='yes'
		[exit_code]='4'
		[verbosity_minlevel]='1'
	)
	__msg "$@"
	#  ^ Exits.
}
export -f  err-ikw


 # The most simple message function.
#
#  It has no colour code and doesn’t display an asterisk or a message type:
#  it’s plain, as the name suggests.
#
#  It still has advantages over the “echo” builtin:
#    - it follows message indentation;
#    - it aligns its message text with the text of other messages, that
#      possess an asterisk (text aligned among messages on the same indenta-
#      tion level).
#  Thus it’s intended to be used for messages of lower importance than info().
#  For example, when printing a list of something to console, the asterisks
#  of info() would only clutter the output or even make it confusing. A plain
#  message function, that respects indentation levels, comes handy here:
#
#      info "Parameters to be set:"
#      milinc
#      for key in ${!parameters[*]}; do
#          msg "$key: ${parameters[$key]}"
#      done
#      mildec
#
msg() { plainmsg "$@"; }
plainmsg() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	declare -A __msg_properties=(
		[message_array]='BAHELITE_INFO_MESSAGES'
		[colour]='PLAIN_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]='  '
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]='3'
	)
	__msg "$@"
	return 0
}
export -f  msg  plainmsg


 # 5 versions of info() and msg() for higher than normal verbosity levels:
#    - info-4, info-5, … info-8;
#    - msg-4, msg-5, … msg-8;
#  the number in the function name corresponds to a verbosity level. It starts
#  with 4, because regular info() and msg() use level 3. (You may think of all
#  message functions as verbosity aliases to __msg(). Thus, err() may as well
#  be named __msg-1(), warn() – __msg-2() and so on.)
#
#  5 functions for extra levels should be enough for the most use cases.
#  If there would be needed 6 or more, copy the cycle below to your code
#  and replace the scope {4..8} with the numbers of your choice, e.g. {9..12}.
#  The minimum is 2, the maximum is 99999. Be sure that the copied code is
#  executed at the global scope.
#
source <(
	for i in {4..8}; do
		cat <<-EOF
		info-$i() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__verbose_msg "\$@"
		}
		msg-$i() {
			bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
			__verbose_msg "\$@"
		}
		export -f  info-$i  msg-$i
		EOF
	done
)
__verbose_msg() {
	local level
	local asterisk
	local ch_name="$(vchan getname)"

	level=${FUNCNAME[1]}
	level=${level#info-}
	level=${level#msg-}
	[[ "$level" =~ ^([4-9]|[1-9][0-9]{1,4})$ ]]  \
		|| err "Aliases to ${FUNCNAME[0]}() must use verbosity level 4 or above."

	 # Saving 15 ms on each info-N call by avoiding call to __msg()
	#  when it will be unnecessary. Thanks to this, Bahelite loads
	#  in 560 ms instead of 1035, or 45% faster.
	#
	(( ${VERBOSITY_CHANNELS[$ch_name]} >= level ))  || return 0

	[[ "${FUNCNAME[1]}" =~ ^info- ]]  \
		&& asterisk="* "  \
		|| asterisk="  "

	[ -v VERBOSITY_MSG_WITH_CHNAME ] && {
		[ "$ch_name" != main ]  \
			&& asterisk+="$ch_name, "
	}
	asterisk+="L$level: "

	declare -A __msg_properties=(
		[message_array]='BAHELITE_DEBUG_MESSAGES'
		[colour]='DEBUG_MESSAGE_COLOUR'
		[whole_message_in_colour]='no'
		[asterisk]="$asterisk"
		[desktop_message]='no'
		[desktop_message_type]=''
		[stay_on_line]='no'
		[output]='stdout'
		[keyworded]='no'
		[exit_code]=''
		[verbosity_minlevel]="passes"
		#  ^ This allows __msg() to skip its verbosity check.
	)
	__msg "$@"
	return 0
}
export -f __verbose_msg


 # A general purpose function to show an info, a warning or an error message
#
#  Applies verbosity and output (log/console/desktop) restrictions, colour
#  codes, message indentation, exiting the program.
#
#  $1 — a text message or, if MSG_USE_KEYWORDS is set, a keyword.
#
__msg() {
	#  Internal! There should be no xtrace_off!
	declare -gx  BAHELITE_STIPULATED_ERROR

	local  message_array
	local  colour
	local  whole_message_in_colour
	local  asterisk
	local  desktop_message
	local  desktop_message_type
	local  stay_on_line
	local  output
	local  keyworded
	local  verbosity_minlevel
	local  exit_code
	local  f
	local  f_count=0
	local  already_printing_call_stack
	local  message=''
	local  message_key
	local  message_key_exists
	local  _message=''
	local  return_or_exit
	local  i

	local -x ast_padding=''


	 # As a precaution against internal bugs, we check how many times __msg()
	#    appears in the call stack. If the number will be more than 3, this
	#    would hint at a recursive error.
	#  It has to be 3, because an error may be triggered within this very
	#    function, and if an error then happens while displaying the error
	#    message, there will be two __msg()  in the call stack. However
	#    this isn’t an erroneous recursion yet, but happen this function
	#    appear one more time (making it three), it will definitely be that.
	#
	for f in "${FUNCNAME[@]}"; do
		[ "$f" = "${FUNCNAME[0]}" ] && let '++f_count,  1'
	done
	(( f_count >= 3 )) && {
		echo "Bahelite error: call to $FUNCNAME has went into recursion."  \
			>&$STDERR_ORIG_FD_PATH
		[ "$(type -t print_call_stack)" = 'function' ]  && {
			#  Print call stack, unless already in the middle of doing it
			for f in "${FUNCNAME[@]}"; do
				[ "$f" = print_call_stack ]  \
					&& already_printing_call_stack=t
			done
			[ ! -v already_printing_call_stack ]  \
				&& print_call_stack
		}
		#  Unsetting the traps, or the recursion may happen again.
		trap '' EXIT TERM INT HUP PIPE ERR DEBUG RETURN
		#  Now the script will exit guaranteely.
		exit 4
	}

	verbosity_minlevel="${__msg_properties[verbosity_minlevel]}"
	[[ "${__msg_properties[exit_code]}" =~ ^[0-9]{1,3}$ ]]  \
		&& exit_code=${__msg_properties[exit_code]}

	 # Hook for the current verbosity channel
	#  It allows
	#    - to suppress messages by default without disabling stdout or stderr.
	#    - to have some parts of code more silent – or vice versa, more
	#      verbose – than the main body of code.
	#
	if	   [ -v BAHELITE_MODULE_VERBOSITY_VER  ]       \
		&& [ "$verbosity_minlevel" != 'passes' ]        \
		&& (( $(vchan getlevel) < verbosity_minlevel ))  \
		&& [ ! -v exit_code ]  #  Makes sure, that err() can always stop
		                       #  the program.
	then
		#  This is a message-level verbosity, it takes priority over the
		#  verbosity level of an output that it was supposed to go to.
		return 0
	fi

	declare -n message_array=${__msg_properties[message_array]}

	[ -v NO_COLOURS ]  \
		|| declare -n colour=${__msg_properties[colour]}

	[ "${__msg_properties[whole_message_in_colour]}" = 'yes' ] \
		&& whole_message_in_colour=${__msg_properties[whole_message_in_colour]}

	asterisk=${__msg_properties[asterisk]}

	[ "${__msg_properties[desktop_message]}" = 'yes' ]  \
		&& desktop_message=${__msg_properties[desktop_message]}

	desktop_message_type=${__msg_properties[desktop_message_type]}

	[ "${__msg_properties[stay_on_line]}" = 'yes' ]  \
		&& stay_on_line=${__msg_properties[stay_on_line]}

	output=${__msg_properties[output]}

	[ "${__msg_properties[keyworded]}" = 'yes' ]  \
		&& keyworded=${__msg_properties[keyworded]}


	 # Setting the message text.
	#  Checking here if the text is in $1, or it has to be found by a keyword.
	#
	if [ -v MSG_USE_KEYWORDS  -o  -v keyworded ]; then
		#  What was passed to us is not a message per se,
		#  but a key in the messages array.
		message_key="${1:-}"
		for key in "${!message_array[@]}"; do
			[ "$key" = "$message_key" ] && message_key_exists=t
		done
		if [ -v message_key_exists ]; then
			#  Positional parameters "$2..n" now can be substituted
			#  into the message strings. To make these substitutions go
			#  from the number 1, drop the $1, holding the message key.
			shift
			eval message=\"${message_array[$message_key]}\"
		else
			err-ikw 'no such msg' "$message_key"
		fi
	else
		# message="${1:-No message?}"
		message="${1:-}"
	fi


	 # Removing blank space before message lines.
	#  This allows strings to be split across lines and at the same time
	#  be well-indented with tabs and/or spaces – indentation will be cut
	#  from the output.
	#
	message=$(sed -r 's/^\s*//; s/\n\t/\n/g' <<<"$message")


	 # Making  desktop_message  hold a copy of $message, so that the two
	#  routes (to console and to desktop) might be handled separately.
	#
	[ -v desktop_message ] && desktop_message="$message"


	 # Logging module hook –
	#  to see, if the output should be suppressed or not. The “console”
	#  channel may have an output disabled, but it may be enabled in “log”,
	#  in which case the message should be sent to its original destination
	#  (stdout or stderr), and the logging redirects will suppress the output
	#  to console as necessary.
	#
	case "$output" in
		stdout)
			vchan minlevel console 2  || {
				[ -v BAHELITE_LOGGING_STARTED ]  \
					&& vchan minlevel log 2  \
					|| output='devnull'
			}
			;;

		stderr)
			vchan minlevel console 1  || {
				[ -v BAHELITE_LOGGING_STARTED ]  \
					&& vchan minlevel log 1  \
					|| output='devnull'
			}
			;;
	esac


	 # Desktop message hook –
	#  to see, if this function should trigger the desktop message or not.
	#
	#  Unlike with “logging” module, where start_logging() has to set
	#  a flag for us to be sure, that the facility is operational, here
	#  we can simply check if the module has defined its presence.
	#
	[ -v desktop_message ] && {
		if	   [ -v BAHELITE_MODULE_MESSAGES_TO_DESKTOP_VER ]      \
			&& [ "$(type -t bahelite_notify_send)" = 'function' ]  \
			#&& [ -v message ]    # was formerly used, not necessary now

		then
			case "$(vchan getlevel 'desktop')" in
				0)	unset desktop_message
					;;

				1)	[ "$desktop_message_type" = 'err' ]  \
						|| unset desktop_message
					;;

				2)	[[ "$desktop_message_type" =~ ^(err|warn)$ ]]  \
						|| unset desktop_message
					;;
			esac
		else
			#  If the module is not loaded (or not loaded yet) –
			#  then disable message to desktop.
			unset desktop_message
		fi
	}


	[ -v message ] && {
		#  Reset any colour alternating codes, that may be in effect.
		_message+="${__stop:-}"

		#  Add colour code (info/warn/err/debug) or an asterisk with
		#  message type when colours are disabled.
		_message+="${colour:-}$asterisk"

		#  If the whole message should be coloured, don’t reset the colour.
		[ -v whole_message_in_colour ]  || _message+="${__stop:-}"

		#  Add message text and colour reset code.
		_message+="$message${__stop:-}"

		#  If colours are disabled, strip them (including any colour codes,
		#  that may be withing the message text itself).
		[ -v NO_COLOURS ]  \
			&&  _message=$(strip_colours "$_message")

		#  Adding message indentation and folding lines.
		#  See the description to MSG_AUTOFOLD_MESSAGES.
		for ((i=0; i<${#asterisk}; i++)); do
			ast_padding+=' '
		done
		if [ -v MSG_AUTOFOLD_MESSAGES ]; then
			export asterisk
			message=$(echo -e ${stay_on_line:+-n} "$_message" \
			              | $__fold -w $((TERM_COLUMNS - ${#__mi} - ${#asterisk})) -s \
			              | sed -r "1s/^/${__mi}/; 1!s/^/$__mi$ast_padding/g" )
			export -n asterisk
		else
			message=$(echo -e ${stay_on_line:+-n} "$_message" \
			              | sed -r "1s/^/${__mi}/; 1!s/^/$__mi$ast_padding/g" )
		fi

		case "$output" in
			stdout)
				if	(( BASH_SUBSHELL == 0 ))  ||  [ ! -v STDOUT_ORIG_FD_PATH ]
				then
					echo ${stay_on_line:+-n} "$message"
				else
					#  If this is the subshell, use the parent shell’s
					#    file descriptors to send messages, because they
					#    shouldn’t be grabbed along with the output.
					#  The parent shell’s FD may be closed, so a check
					#    is needed to confirm, that it’s still writeable.
					[ -w "$STDOUT_ORIG_FD_PATH" ]  \
						&& echo ${stay_on_line:+-n} "$message"   \
						        >$STDOUT_ORIG_FD_PATH
				fi
				;;

			stderr)
				if	(( BASH_SUBSHELL == 0 ))  ||  [ ! -v STDOUT_ORIG_FD_PATH ]
				then
					echo ${stay_on_line:+-n} "$message" >&2

				else
					#  If this is the subshell, use the parent shell’s
					#    file descriptors to send messages, because they
					#    shouldn’t be grabbed along with the output.
					#  The parent shell’s FD may be closed, so a check
					#    is needed to confirm, that it’s still writeable.
					[ -w "$STDERR_ORIG_FD_PATH" ]  \
						&& echo ${stay_on_line:+-n} "$message"   \
						        >$STDERR_ORIG_FD_PATH
				fi
				;;

			devnull)
				:  #  Not sending anything
				;;
		esac
	}

	[ -v desktop_message ]  && {
		bahelite_notify_send "$(strip_colours "$desktop_message")"  \
		                     "$desktop_message_type"
	}

	 # Triggering return or exit for error-level messages.
	#
	[ -v exit_code ] && {
		BAHELITE_STIPULATED_ERROR=t

		if (( BASH_SUBSHELL > 0 )); then
			#  In the short period during loading modules, an error may happen
			#  between loading this module and “tmpdir”, in which case TMPDIR
			#  wouldn’t be set yet.
			touch "${TMPDIR:-/tmp}/BAHELITE_STIPULATED_ERROR_IN_SUBSHELL"
			return_or_exit='exit'
		else
			return_or_exit='return'
		fi


		 # If this is an error/abort message, the program must quit
		#  with a certain exit code.
		#
		#  /return/ should be used here – not exit. Usage of “exit” breaks
		#  the consistency of the call stack in cases when one sourced script
		#  calls a function from another sourced script, and an error is
		#  triggered from there. Call stack would become abrupt with just
		#  the “source” in FUNCNAME, rendering it almost useless.
		#
		if	[ -v keyworded ]
		then
			$return_or_exit $exit_code
		elif  [ -v MSG_USE_KEYWORDS ]  \
		      &&  bahelite_validate_error_code "${ERROR_CODES[$*]}"
		then
			$return_or_exit ${ERROR_CODES[$*]}
		else
			$return_or_exit $exit_code
		fi
	}
	return 0
}
export -f  __msg


 # A divider is a message, that is printed highlighted and takes the entire
#  line. It is intended to improve readability for the cases, when the output
#  of the main script is temporarily suspended, and another program prints
#  to console: for this reason the divider message also increases and decreases
#  the message indentation level accordingly.
#
headermsg() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	milinc
	#  Bad idea: to add a spacing “echo”.
	#  It would confuse the one reading the inner output (probably a log)
	#    with a question “Does this space belong to the log? Is this what
	#    causes an error?”
	divider_message "$@"
	return 0
}
export -f headermsg


footermsg() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	divider_message "$@"
	#  No spacing echo for the same reason as above.
	mildec
	return 0
}
export -f footermsg


 # Print a message, that will span over entire line
#  $1  – text message.
# [$2] – character to use for the divider line. If unspecified, set to “+”.
# [$3] – style to use for the line. If unspecified, uses whatever is set
#        in the $HEADER_MESSAGE_COLOUR variable.
#
divider_message() {
	#  Internal! There should be no xtrace_off!
	local  message="$1"
	local  divider_line_character="${2:-+}"
	local  style="${3:-$HEADER_MESSAGE_COLOUR}"
	local  line_to_print=''
	local  line_to_print_length
	local  i

	(( MSG_INDENTATION_LEVEL > 0 ))  && line_to_print+="$__mi"
	line_to_print+="$style"
	for	(( i=0; i<3; i++ )); do
		line_to_print+="$divider_line_character"
	done
	line_to_print+=" $message "
	if [ -v NO_COLOURS ]; then
		line_to_print_length=${#line_to_print}
	else
		line_to_print_length="$(strip_colours "$line_to_print")"
		line_to_print_length=${#line_to_print_length}
	fi
	for	(( i=0;  i < TERM_COLUMNS - line_to_print_length;  i++ )); do
		line_to_print+="$divider_line_character"
	done
	line_to_print+="${__s:-}"
	if (( BASH_SUBSHELL == 0 )); then
		[ "${FUNCNAME[1]}" = headermsg ] && echo
		echo -e "$line_to_print"
		[ "${FUNCNAME[1]}" = footermsg ] && echo
	else
		#  If this is the subshell, use the parent shell’s
		#    file descriptors to send messages, because they
		#    shouldn’t be grabbed along with the output.
		#  The parent shell’s FD may be closed, so a check
		#    is needed to confirm, that it’s still writeable.
		[ -w "$STDOUT_ORIG_FD_PATH" ] && {
			[ "${FUNCNAME[1]}" = headermsg ] && echo  >$STDOUT_ORIG_FD_PATH
			echo -e "$line_to_print"  >$STDOUT_ORIG_FD_PATH
			[ "${FUNCNAME[1]}" = footermsg ] && echo  >$STDOUT_ORIG_FD_PATH
		}
	fi
	return 0
}
export -f divider_message



return 0



 # NOTES
#
#  There wasn’t made an alias like “dbg” or “debug” to info-4 (as a simple
#  name to have a fifth type of message function with a distinctive name and
#  purpose), because it would look inconsistent in conjuction with msg-4,
#  which cannot be shortened (msg() exists and uses verbosity level 3).
#
#  Aliases to __verbose_msg() could bear numbers starting with 2 (and not
#  with 4), but if these functions would then be used among working with
#  actual verbosity levels (e.g. for speed), such a shift would make the code
#  rather confusing to read. So it was decided to stick to the actual verbo-
#  sity levels (starting with 4) in function numbers/indices.
#
