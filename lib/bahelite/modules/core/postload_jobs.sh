#  Should be sourced.

#  postload_jobs.sh
#  Validate and run the functions (aka jobs), that modules have set up to
#  run after everything is loaded (that is, after all the source code of the
#  modules is loaded). See “load_modules” module for the details.
#  © deterenkelt 2019–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_POSTLOAD_JOBS_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_POSTLOAD_JOBS_VER='1.0.3'

#  BAHELITE_POSTLOAD_JOBS is defined in the “load_modules” module, as it has
#  to be defined before the other modules would start to extend it.

vchan setup '!#Postload_jobs=2'


show_help_on_verbosity_channel_Postload_jobs() {
	cat <<-EOF
	Postload_jobs (self-report channel)
	    Describes the process of running jobs, postponed until the time all
	    modules would load. The jobs are created by modules.
	    Levels:
	    2 – no additional output (the default);
	    3 – show a message when a module is going to be loaded; if “time-stat”
	        option was used on the module “bahelite”, also measure time that
	        the module loads;
	    4 – in addition, print the options for the module being loaded.

	To see the details on the process of loading modules, set level 4 to
	the “Load_modules” channel, and perhaps, to a specific module’s channel
	of your interest.
	EOF
	return 0
}


__show_usage_postload_jobs_module() {
	cat <<-EOF
	Bahelite module “postload_jobs” doesn’t take options!
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_postload_jobs_module
		exit 0

	else
		redmsg "Bahelite: postload_jobs: unrecognised option “$opt”."
		__show_usage_postload_jobs_module
		exit 4
	fi
done
unset opt



 # When all modules are loaded, it’s time to run their “startup jobs”.
#
#  Loading a module simply defines the code of the necessary facilities,
#  but to do actual job (to prepare a directory in $HOME or to start a log)
#  a particular function has to be called. Modules define the names of such
#  functions in the BAHELITE_POSTLOAD_JOBS array.
#
#  This function runs the jobs in the requested order and resolves their
#  dependencies (calls other functions) as needed.
#
#  $1..n – job specifications.
#
#          Job specification format is one of the following:
#          “func_name” – runs the specified Bahlite function
#          “func_name:after=another_func” – requests, that “func_name” is
#              to be called after “another_func”.
#          “func_name:after=func1,func2, … funcN” – requests that all func-
#              tions specified after the keyword “:after=” are to be executed
#              prior to executing func_name.
#
#          Uses "${BAHELITE_POSTLOAD_JOBS[@]}" for arguments.
#
bahelite_run_postload_jobs() {
	#  This way we shorten the load time from ≈990 ms to ≈300 ms
	local verbosity_depth=${VERBOSITY_CHANNELS[Postload_jobs]}

	(( verbosity_depth >= 3 )) && {
		info "Running postload jobs."
		milinc
	}

	 # Job list as is.
	#
	local jobs=( "$@" )

	 # When the dependencies are being resolved, this array holds the current
	#  chain of function names. This list is used to prevent an accidental
	#  recursive dependency.
	#
	local execution_chain=()

	 # During the recursive call, the function, that resolves dependencies,
	#  adds the names of completed functions to this array. Thus on the next
	#  iteration (or when the execution returns from yet another loop) these
	#  functions – which may be required as dependencies to other, not yet
	#  resolved jobs – these functions will be known as already executed
	#  once, i.e. as a solved dependency.
	#
	local resolved_deps=()


	 # Resolves dependencies for a job and runs it.
	#
	#  Functions specified as dependencies must be loaded beforehand.
	#    The “:after=” keyword doesn’t forcibly find and load a dependency
	#    function, if it isn’t present in the scope.
	#  Remember, that modules are first loaded, and that’s only after all the
	#    source code is loaded, the actual stuff runs.
	#  Modules are responsible for preloading other modules and for the pre-
	#    sence in the scope of all functions that a module requires. Modules
	#    thus load some other modules always (as a hard dependency), and some
	#    modules-dependencies are loaded only when a a module receives a spe-
	#    cific option via BAHELITE_LOAD_MODULES. Such modules as “logging”
	#    and “rcfile” provide an example of how this is done.
	#
	#  $1 – job specification (as passed to the mother function).
	#
	__resovle_deps_and_run() {
		local jobspec="$1"
		local job_func_name=''
		local job_deps_list=()
		local dep_func

		(( verbosity_depth >= 4 )) && {
			info-4 "Job “$jobspec”"
			milinc
		}

		if [[ "$jobspec" =~ ^([A-Za-z0-9_-]+)\:after\=(.*)$ ]]; then
			job_func_name="${BASH_REMATCH[1]}"
			job_deps_list=( ${BASH_REMATCH[2]//\,/ } )
		else
			job_func_name="$jobspec"
			job_deps_list=()
		fi

		__check_for_a_circular_dependency() {
			local new_job="$1"
			local funcname

			for funcname in "${execution_chain[@]}"; do
				[ "$funcname" = "$new_job" ] && {
					err "Bahelite error: circular dependency in postload jobs.
					     $(for chain_el in "${execution_chain[@]}"; do
					           echo -n "$chain_el —< "
					       done)$new_job"
				}
			done

			return 0
		}

		__is_among_complete() {
			local current_job_or_dep_funcname="$1"
			local funcname

			for funcname in "${resolved_deps[@]}"; do
				[ "$funcname" = "$current_job_or_dep_funcname" ] && {
					(( verbosity_depth >= 4 ))  \
						&& info-4 "$funcname(): already complete!"
					return 0
				}
			done

			return 1
		}

		__is_among_not_present() {
			local current_job_or_dep_funcname="$1"
			local funcname

			for funcname in "${job_deps_list[@]}"; do
				if	[ "$funcname" = "$current_job_or_dep_funcname" ]  \
					&& ! type -t "$funcname" &>/dev/null
				then
					(( verbosity_depth >= 4 ))  \
						&& info-4 "$funcname(): not present, nothing to run!
						           (The module, for which the dependency is placed,
						           may be not loaded, because it’s optional, or it
						           may be because the function name is wrong and
						           the one that must be called, would never run.
						           Unless you’re in the process of debugging some
						           trouble, there’s nothing to worry about. But
						           if you’re looking for a possibel error, this
						           may be the reason.)"
					return 0
				fi
			done

			return 1
		}

		__is_itself_a_job_with_deps() {
			local current_dep_funcname="$1"
			local job_spec

			for job_spec in "${jobs[@]}"; do
				[[ "$job_spec" =~ ^$current_dep_funcname\:after\=.*$ ]]  \
					&& return 0
			done

			return 1
		}

		__find_jobspec_by_funcname() {
			local current_dep_funcname="$1"
			local job_spec

			for job_spec in "${jobs[@]}"; do
				[[ "$job_spec" =~ ^$current_dep_funcname(\:after\=.*)?$ ]]  \
					&& echo "${BASH_REMATCH[0]}"
			done

			return 0
		}

		__is_among_complete "$job_func_name"  && return 0

		__check_for_a_circular_dependency "$job_func_name"
		execution_chain+=( "$job_func_name" )

		(( verbosity_depth >= 4 )) && {
			info-4 "Resolving dependencies…"
			milinc
		}

		for dep_func in "${job_deps_list[@]}"; do
			if	! (  __is_among_complete "$dep_func"  \
				     || __is_among_not_present "$dep_func"  )
			then
				if __is_itself_a_job_with_deps "$dep_func"; then
					__resolve_deps_and_run "$(__find_jobspec_by_funcname "$dep_func")"
				else
					(( verbosity_depth >= 4 ))  \
						&& info-4 "Running $dep_func()…"
					$dep_func
					(( verbosity_depth >= 4 ))  \
						&& info-4 "Postload dependency “$dep_func” is solved."
					resolved_deps+=( "$dep_func" )
				fi
			fi
		done

		(( verbosity_depth >= 4 )) && {
			info-4 "All dependencies resolved!"
			mildec

			info-4 "Running $job_func_name()…"
			milinc
		}
		$job_func_name
		(( verbosity_depth >= 4 ))  \
			&& mildec

		(( verbosity_depth >= 4 ))  && {
			local jobtype
			[ "${FUNCNAME[1]}" = 'bahelite_run_postload_jobs' ]  \
				&& job_or_subjob='job'  \
				|| job_or_subjob='subjob'
			info-4 "Postload $job_or_subjob “$job_func_name” is complete."
			echo
		}

		(( verbosity_depth >= 4 )) && mildec
		resolved_deps+=( "$job_func_name" )
		return 0
	}

	(( verbosity_depth >= 3 )) && {
		if (( ${#BAHELITE_POSTLOAD_JOBS[*]} == 0 )); then
			info "POSTLOAD JOBS list is empty."
		else
			info "POSTLOAD JOBS list:"
			milinc
			for postload_job in "${BAHELITE_POSTLOAD_JOBS[@]}"; do
				msg "$postload_job"
			done
			mildec
		fi
	}

	(( ${#jobs[*]} > 0 )) && {
		(( verbosity_depth >= 3 )) && {
			info "Resolving dependencies of POSTLOAD JOBS list."
			milinc
		}

		local job

		for job in "${jobs[@]}"; do
			#  Start resolving deps for each job with an empty chain.
			execution_chain=()
			if [ $verbosity_depth -ge 3  -a  -v RUN_TIME_STATISTICS ]; then
				update_timeformat
				time __resovle_deps_and_run "$job"
			else
				__resovle_deps_and_run "$job"
			fi
		done
		(( verbosity_depth >= 3 )) && mildec
	}

	(( verbosity_depth >= 3 )) && mildec
	return 0
}
#  No export: init stage function


bahelite_validate_postload_jobs() {
	local  jobs=( "$@" )
	local  verbosity_depth=${VERBOSITY_CHANNELS[Postload_jobs]}

	(( verbosity_depth >= 3 ))  && {
		info "Validating $# specifications in BAHELITE_POSTLOAD_JOBS."
		milinc
	}

	local funcname='[A-Za-z0-9_-]+'
	local job

	for job in "${jobs[@]}"; do
		[[ "$job" =~ ^$funcname(\:after\=$funcname(\,$funcname)*)?$ ]]  \
			|| err "Bahelite error: invalid postload job specification:
			          “$job”."
	done

	(( verbosity_depth >= 3 ))  && {
		info 'All is OK.'
		mildec
	}

	return 0
}
#  No export: init stage function



return 0