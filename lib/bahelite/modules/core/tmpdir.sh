#  Should be sourced.

#  tmpdir.sh
#  Set up a temporary directory for the main script and set TMPDIR variable.
#  The path would be /tmp/$MYNAME_NOEXT.XXXXXXXXXX
#  © deterenkelt 2019–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_TMPDIR_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_TMPDIR_VER='1.1'

 # Facility’s verbosity channel
#  0 – the default operational mode
#  1 – verbose mode: do not remove the directory for temporary files on exit.
#
#      This is implemented as a verbosity level (and not as a module option),
#      because the need to leave TMPDIR would happen only occasionally. The
#      end users would have less troubles by accessing this feature via non-
#      intrusive means like changing verbosity level, instead of having to
#      edit the code of the program. (That action may be dangerous for non-
#      professionals, and the necessity to revert the changes thereafter
#      makes it double dangerous for them. In case when updates are performed
#      via “git pull” it would be critical to have a clean, non-edited state
#      for the files.)
#
vchan setup '*!#tmpdir=0|!0'

(( ${VERBOSITY_CHANNELS[tmpdir]} == 1 ))  \
	&& BAHELITE_DONT_CLEAR_TMPDIR=t

show_help_on_verbosity_channel_tmpdir() {
	cat <<-EOF
	tmpdir
	    Controls the persistence of TMPDIR ($MYNAME’s directory
	    for temporary files).
	    Levels:
	    0 – delete TMPDIR on exit (the default).
	    1 – keep TMPDIR on exit.
	EOF
}


 # Module’s self-report verbosity channel
#
vchan setup '!#Tmpdir=2'

show_help_on_verbosity_channel_Tmpdir() {
	cat <<-EOF
	Tmpdir (self-report channel)
	    Levels:
	    2 – no additional messages (the default)
	    3 – unused
	    4 – show details on how the directory is created and deleted.

	EOF
}

__show_usage_tmpdir_module() {
	cat <<-EOF
	Bahelite module “tmpdir” doesn’t take options!
	Tips:
	  - to set a custom path, export it in TMPDIR variable;
	  - to avoid deletion TMPDIR on exit, export VERBOSITY=tmpdir=1.
	EOF
	return 0
}

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_tmpdir_module
		exit 0

	else
		redmsg "Bahelite: tmpdir: unrecognised option “$opt”."
		__show_usage_tmpdir_module
		exit 4
	fi
done
unset opt


 # Flag to prevent the deletion of TMPDIR on exit.
#  Useful to debug errors or to find logs, when they couldn’t be written
#  to their proper place. This variable is automatically set in subshells,
#  so that upon exit from them TMPDIR wouldn’t get accidentally deleted.
#
#  You don’t have to use this variable, since Bahelite 3.2 you may simply
#  raise verbosity on tmpdir up to 1.
#
#  See also BAHELITE_DONT_CLEAR_TMPDIR_ON_ERROR in “error_handling” module.
#
declare -g BAHELITE_DONT_CLEAR_TMPDIR



 # Create a unique subdirectory for temporary files
#
#  It’s used by Bahelite and the main script. bahelite_on_exit() will call
#    this function’s counterpart, bahelite_delete_tmpdir() to remove the
#    directory.
#  Use TMPDIR for small files. For larger ones (1+ GiB) you should load
#    module “runtimedir”, that will create a directory under XDG_RUNTIME_DIR
#    and place the big files there.
#  You may set TMPDIR before sourcing Bahelite, this way the set directory
#    will be used instead.
#
create_tmpdir() {
	declare -gx TMPDIR

	local  verbosity_depth=${VERBOSITY_CHANNELS[Tmpdir]}

	(( verbosity_depth >= 4 )) && {
		info-4 "Setting TMPDIR…"
		milinc
		info-4 'Choosing base directory for temporary files'
		milinc
	}

	[ -v TMPDIR ] && {
		(( verbosity_depth >= 4 )) && {
			info-4 'TMPDIR is set in the environment, trying to use it.'
			milinc
		}
		if [ -d "${TMPDIR:-}" ]; then
			(( verbosity_depth >= 4 ))  \
				&& msg-4 "directory is OK, using “$TMPDIR” as a base."
		else
			warn "tmpdir: no such directory: “$TMPDIR”."
			unset TMPDIR
		fi
		(( verbosity_depth >= 4 )) && mildec
	}
	(( verbosity_depth >= 4 )) && {
		mildec-4
		info-4 "Creating a temporary directory withn base directory.
		        Base directory = ${TMPDIR:-/tmp}"
	}
	TMPDIR=$(mktemp --tmpdir=${TMPDIR:-/tmp/}  -d $MYNAME_NOEXT.XXXXXXXXXX  )
	#  bahelite_on_exit trap shouldn’t remove TMPDIR, if the exit occurs
	#  within a subshell
	(( BASH_SUBSHELL > 0 )) && BAHELITE_DONT_CLEAR_TMPDIR=t
	(( verbosity_depth >= 4 ))  \
		&& info-4 "Setting TMPDIR to $TMPDIR"

	declare -r TMPDIR
	(( verbosity_depth >= 4 )) && mildec
	return 0
}
#  No export: init stage function.


 # For bahelite_on_exit() which is called in error_handling.sh.
#
delete_tmpdir() {
	local  verbosity_depth=${VERBOSITY_CHANNELS[Tmpdir]}

	#  mountpoint is a apart of coreutils, so it doesn’t have to be
	#  specified in BAHELITE_INTERNALLY_REQUIRED_UTILS
	(( verbosity_depth >= 4 ))  && {
		info-4 'Deleting TMPDIR…'
		milinc
	}

	if    [ -d "$TMPDIR" ]  \
	   && [ ! -v BAHELITE_DONT_CLEAR_TMPDIR ]  \
	   && ! mountpoint --quiet "$TMPDIR"
	then
		#  Remove TMPDIR only after logging is done.
		rm -rf "$TMPDIR"
		(( verbosity_depth >= 4 ))  \
			&& info-4 'Deleted.'
	else
		(( verbosity_depth >= 4 ))  \
			&& info-4 'Kept in place.'
	fi

	(( verbosity_depth >= 4 )) && mildec
	return 0
}
#  No export: to be executed only once and only in the top level shell.



#  Needed ASAP.
create_tmpdir


return 0