#  Should be sourced.

#  verbosity.sh
#  Facility to use and create verbosity channels, that add an ability to con-
#  trol, how silent – or vice versa, verbose – is the output of the entire
#  program or a specific part of it.
#  © deterenkelt 2019–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_VERBOSITY_VER ] && return 0
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_VERBOSITY_VER='2.0.1'

#  Option processing is done in “verbosity_base” module.
#  Channels are set up below function definitions.



           #  Verbosity module internal functions for vchan()  #

 # Note, these functions are strictly internal: they are dumb and do exactly
#    what their names say. This means that they do not perform any validation,
#    a function on a higher level – vchan() – does that for them.
#  There indeed are all the required validation functions among the internals.
#    But they are not used in the other internals, as there is no telling
#    how much times something would be re-validated. So checking the input
#    is relayed to vchan().
#  And there are subroutines of vchan() itself, they don’t count as internals
#    (they aren’t general-purpose and strictly bound to a specific vchan com-
#    mand), and perform validation themselves.
#


 # Checks if a string is a proper verbosity channel name.
#  $1  – string to check.
#
__validate_verbosity_channel_name() {
	local  ch_name="$1"
	local  name_pat='[A-Za-z_][A-Za-z0-9_-]*'

	[[ "$ch_name" =~ ^$name_pat$ ]]  || {
		redmsg "“$ch_name” isn’t a valid verbosity channel name.
		        Run the script with --help-verbosity for description."
		err-stack 'Incorrect name for verbosity channel.'
	}

	return 0
}
export -f __validate_verbosity_channel_name


 # Checks if a string is a proper number for a verbosity level.
#  $1 – string to check.
#
__validate_verbosity_channel_level() {
	local  ch_level="$1"
	local  level_pat='([0-9]|[1-9][0-9]{0,4})'

	[[ "$ch_level" =~ ^$level_pat$ ]]  || {
		#  Could simply say that valid values are 0…99999, but it’s better
		#  to direct unknowledgeable people to the tip at least once.
		redmsg "“$ch_level” isn’t a valid level for verbosity channel.
		        Run the script with --help-verbosity for description."
		err-stack 'Incorrect value for verbosity level.'
	}

	return 0
}
export -f __validate_verbosity_channel_name


 # Returns the type of the specified channel name or group name.
#  - «channel» for regular (standalone) channels.
#  - «group» if the name defines a group
#  - «channel in group “GROUPNAME”» if the specified name was found
#    belonging to a group, whose name would be returned in double quotes.
#  - «doesn’t exist» if the specified name wasn’t found anywhere.
#  Types “channel” and “group” may have a prefix “meta-”, indicating that
#  the name of the channel/group itself shouldn’t match against an existing
#  function of the same name (happen there to be one).
#
#  Running this function is costly, and speed-oriented parts of the module
#  (i.e. those involved in level retrieval) better avoid calls to it.
#
#  $1 – channel name or group name. Can be an item within a group.
#       Cannot be a groupspec.
#
__get_verbosity_channel_type() {
	local  test_name="$1"
	local  type=''
	local  vchan_name
	local  vchan_items
	local  name_pat='[A-Za-z_][A-Za-z0-9_-]*'

	[ -v VERBOSITY_CHANNELS[$test_name] ]  || {
		echo 'doesn’t exist'
		return 0
	}

	for vchan_name in ${!VERBOSITY_CHANNELS_GROUP_ITEMS[*]}; do
		vchan_items="${VERBOSITY_CHANNELS_GROUP_ITEMS[$vchan_name]}"
		[[ "$vchan_items" =~ ^($name_pat,)*$test_name(,$name_pat)*$ ]] && {
			echo "channel in group “$vchan_name”"
			return 0
		}
	done

	[ -v VERBOSITY_CHANNELS_META[$test_name] ]  \
		&& type='meta-'

	[ -v VERBOSITY_CHANNELS_GROUP_ITEMS[$test_name] ]  \
		&& type+='group'  \
		|| type+='channel'

	echo "$type"
	return 0
}
export -f __get_verbosity_channel_type


 # Retrieves the name of the verbosity channel that is currently in effect
#  (that is, at the place in the code from where “vchan”, calling this func-
#  tion, was called.)
#
__get_current_verbosity_channel_name() {
	local  i
	local  func_name
	local  found_chname

	for ((i=2; i<${#FUNCNAME[@]}; i++)); do
		func_name=${FUNCNAME[i]}
		if	   [   -v VERBOSITY_CHANNELS[$func_name]      ]  \
			&& [ ! -v VERBOSITY_CHANNELS_META[$func_name] ]
			#  ^ there is no need to check the master’s meta in case that’d be
			#    a slave channel – slave channels can never be meta, they are
			#    supposed to always match function names.
		then
			found_chname="$func_name"
			break
		fi
	done

	if [ -v found_chname ]; then
		echo "$found_chname"
		(( ${VERBOSITY_CHANNELS[Verbosity]} >= 5 ))  && {
			(
			set +T
			echo "${__mi}verbosity: cur. channel is “$found_chname”"
			echo -n "${__mi}  "
			sed -r "s/ /\n${__mi}  ← /g"  <<<"${FUNCNAME[*]}"
			) >&${STDOUT_ORIG_FD_PATH:-/proc/$$/fd/1}
		}
	else
		err-stack "Couldn’t determine current verbosity channel. No “main”?"
	fi
	return 0
}
export -f __get_current_verbosity_channel_name


 # Sets verbosity level for the specified channel.
#  $1 – channel name. Can be a (meta-)channel or (meta-)group, but cannot be
#       a group item. If the channel is a group, then also sets the specified
#       level to its group items.
#  $2 – level, 0…99999
#
__set_verbosity_level() {
	local  ch_name="$1"
	local  level="$2"
	local  ch_type
	local  group_items
	local  slave_ch_name

	[ -v VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name] ]  && {
		ch_level=${VERBOSITY_CHANNELS[$ch_name]}
		lock_level=${VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name]}
		(( ch_level >= lock_level )) && {
			redmsg "Setting verbosity level on “$ch_name” now is useless, because
			        the changes made at the level “$lock_level” are irreversible.
			        To pass the level as a preset, use the environment variable
			        VERBOSITY or the command line parameter --verbosity."
	        err "Cannot set verbosity level on channel “$ch_name” – it’s futile."
		}
	}

	ch_type="$(__get_verbosity_channel_type "$ch_name")"
	case "$ch_type" in
		group|meta-group)
			declare -n group_items="VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]"
			for slave_ch_name in ${group_items//,/ }; do
				VERBOSITY_CHANNELS[$slave_ch_name]=$level
			done
			;&
		channel|meta-channel)
			VERBOSITY_CHANNELS[$ch_name]=$level
			;;
		channel\ in\ group*)
			[[ "$ch_type" =~ “([A-Za-z_][A-Za-z0-9_-]*)” ]]
			redmsg "Changing verbosity level on a channel, that belongs to a group
			        is forbidden. Instead, set the level for its master channel."
			err "Cannot set verbosity level on channel “$ch_name” –
			     the channel belongs to group “${BASH_REMATCH[1]}”."
			;;
	esac

	return 0
}
export -f __set_verbosity_level


 # Subroutine of  __set_up_verbosity_channel().
#
__describe_how_meta_changes() {
	local _msg

	(( verbosity_depth >= 4 )) && {
		_msg="“Meta” "
		[ -v meta_enforced ]  \
			&& _msg+='overrides preset: '  \
			|| _msg+='yields to preset: '
		[ -v exists_as_meta ]  \
			&& _msg+='on  '  \
			|| _msg+='off  '
		if	   [ -v exists_as_meta  -a  -v meta ]  \
			|| [ ! -v exists_as_meta  -a  ! -v meta ]
		then
			_msg+='(no change).'
		else
			if [ -v meta_enforced ]; then
				[ -v meta ]  \
					&& _msg+='→ on'  \
					|| _msg+='→ off'
			else
				[ -v exists_as_meta ]  \
					&& _msg+='→ on.'  \
					|| _msg+='→ off.'
			fi
		fi
		msg "$_msg"
	}

	return 0
}
export -f __describe_how_meta_changes


 # Subroutine of  __set_up_verbosity_channel().
#
__describe_how_level_changes() {
	local _msg
	local old_level

	(( verbosity_depth >= 4 )) && {
		old_level=${VERBOSITY_CHANNELS[$ch_name]}
		_msg="Level "
		[ -v level_enforced ]  \
			&& _msg+='overrides preset: '  \
			|| _msg+='yields to preset: '
		_msg+="$old_level  "
		if [ -v level_enforced ]; then
			(( level == old_level ))  \
				&& _msg+='(no change).'  \
				|| _msg+="$level."
		else
			_msg+='(no change).'
		fi
		msg "$_msg"
	}

	return 0
}
export -f __describe_how_level_changes


 # Subroutine of  __set_up_verbosity_channel().
#
__describe_how_locklevel_changes() {
	local  _msg
	local  old_locklevel

	(( verbosity_depth >= 4 )) && {
		[ -v VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name] ]  \
			&& old_locklevel=${VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name]}
		_msg="Locklevel "
		[ -v lock_on_level_enforced ]  \
			&& _msg+='overrides preset: '  \
			|| _msg+='yields to preset: '
		[ -v old_locklevel ]  \
			&& _msg+="$old_locklevel  "  \
			|| _msg+="(no locklevel)  "
		if [ -v lock_on_level_enforced ]; then
			[ -v lock_on_level ]  \
				&& _msg+="→ $lock_on_level."  \
				|| _msg+="→ to be deleted."
		else
			_msg+="(no change)."
		fi
		msg "$_msg"
	}

	return 0
}
export -f __describe_how_locklevel_changes


 # Subroutine of  __set_up_verbosity_channel().
#
__describe_how_channel_changes() {
	(( verbosity_depth >= 4 )) && {
		if [ -v group_items ]; then
			msg 'Channel is going to be converted to a group.'
		else
			msg 'Channel will remain a regular channel.'
		fi
	}

	return 0
}
export -f __describe_how_channel_changes


 # Subroutine of  __set_up_verbosity_channel().
#
__describe_how_group_changes() {
	local _msg

	(( verbosity_depth >= 4 )) && {
		if [ -v force_conversion_to_channel ]; then
			info 'The group is going to be converted to channel.
			      Slave channels will cease to exist.'
			return 0
		else
			info 'The group will remain a group.'
		fi
		_msg='The items '
		if [ -v group_items ]; then
			[ -v group_items_enforced ]  \
				&& _msg+='will be replaced with: '  \
				|| _msg+='will be extended with additional items: '
		else
			_msg+='of the original set will be left untouched: '
		fi
		_msg+="$(IFS=' '; echo "${group_items[*]}")."
		msg "$_msg"
	}

	return 0
}
export -f __describe_how_group_changes


 # Creates a verbosity channel or a group from a specification.
#  $1 – specification. See format in the description to “vchan setup”,
#       from where this function is called.
#
__set_up_verbosity_channel() {
	local  input="$1"
	local  gates_an_output
	local  meta
	local  meta_enforced
	local  ch_name
	local  force_conversion_to_channel
	local  group_items
	local  group_items_enforced
	local  gr_item
	local  level
	local  level_enforced
	local  lock_on_level
	local  lock_on_level_enforced
	local  ch_type
	local  to_be_chtype
	local  name_pat='[A-Za-z_][A-Za-z0-9_-]*'
	local  level_pat='([0-9]|[1-9][0-9]{0,4})'
	local  exists_as_meta
	local  slave_ch_name

	(( verbosity_depth >= 3 )) && {
		info "Setting up verbosity channel: “$input”."
		milinc
	}

	 # Hopefully, this can be done without getopt(s), as neither authors
	#  nor lest their users shall call “vchan setup” often. Getopt would
	#  probably add a great lag to the loading process.
	#
	#                                                 Extra parentheses from level_pat!  ↖↘                   ↖↘
	if [[ "$input" =~ ^(\*?)(!?)(#?)($name_pat)(!?)(:(!?)($name_pat(,$name_pat)*)|)(=(!?)($level_pat)|)(\|(!?)($level_pat)|)$ ]]; then
		[ "${BASH_REMATCH[1]}" ] && gates_an_output=t
		[ "${BASH_REMATCH[3]}" ] && meta="${BASH_REMATCH[3]}"
		[ "${BASH_REMATCH[2]}" ] && meta_enforced=t
		ch_name=${BASH_REMATCH[4]}
		[ "${BASH_REMATCH[5]}" ] && force_conversion_to_channel=t
		[ "${BASH_REMATCH[8]}" ] && group_items=${BASH_REMATCH[8]}
		[ "${BASH_REMATCH[7]}" ] && group_items_enforced=t
		level=${BASH_REMATCH[12]:-$VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS}
		[ "${BASH_REMATCH[11]}" ] && level_enforced=t
		[ "${BASH_REMATCH[16]}" ] && lock_on_level=${BASH_REMATCH[16]}
		[ "${BASH_REMATCH[15]}" ] && lock_on_level_enforced=t
	else
		redmsg "Wrong specification for setting up a verbosity channel:
		        $input"
		err-stack 'Cannot create verbosity channel: invalid format.'
	fi

	[ -v force_conversion_to_channel  -a  -v group_items ]  \
		&& err-stack "Cannot setup verbosity channel: impossible to have group items
		              and force conversion to a channel at the same time."

	#  For messages.
	[ -v group_items ]  \
		&& to_be_chtype="${meta:+meta-}group"  \
		|| to_be_chtype="${meta:+meta-}channel"

	 # Check, that the group items, that we’re going to add or set, are not
	#  existing channels, groups or group items.
	#
	[ -v group_items ] && {
		for gr_item in ${group_items//,/ }; do
			[ -v VERBOSITY_CHANNELS[$gr_item] ] && {
				ch_type="$(__get_verbosity_channel_type "$gr_item")"
					err "Cannot create verbosity group with an item “$gr_item”:
					     it already exists as a $ch_type."
			}
		done
	}

	 # Dealing with fresh channels quickly to speed up loading process:
	#  checking existence manually and quitting when the job’s done.
	#  Thus time-consuming type checking is left for the cases when we alter
	#  already existing channels.
	#
	[ ! -v VERBOSITY_CHANNELS[$ch_name] ]  && {
		VERBOSITY_CHANNELS+=( [$ch_name]=$level )
		[ -v gates_an_output ]  \
			&& VERBOSITY_CHANNELS_GATES_AN_OUTPUT[$ch_name]='*'
		[ -v group_items ]  && {
			VERBOSITY_CHANNELS_GROUP_ITEMS+=( [$ch_name]="$group_items" )
			for slave_ch_name in ${group_items//,/ }; do
				VERBOSITY_CHANNELS[$slave_ch_name]=$level
			done
		}
		[ -v meta ]  \
			&& VERBOSITY_CHANNELS_META+=( [$ch_name]='#' )
		[ -v lock_on_level ]  \
			&& VERBOSITY_CHANNELS_LOCK_ON_LEVEL+=( [$ch_name]=$lock_on_level )
		[ -v VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name] ]  \
			&& unset VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name]
		(( verbosity_depth >= 3 ))  && {
			info "${to_be_chtype^} “$ch_name” created."
			mildec
		}
		return 0
	}

	ch_type="$(__get_verbosity_channel_type "$ch_name")"
	case "$ch_type" in
		meta-channel|meta-group)
			exists_as_meta=t
			;&
		channel|group)
			#  Internal cosmetic option, autoforced.
			[ -v gates_an_output ]  \
				&& VERBOSITY_CHANNELS_GATES_AN_OUTPUT[$ch_name]='*'

			__describe_how_meta_changes
			[ -v meta_enforced ] || {
				[ -v exists_as_meta ]  \
					&& meta='#'  \
					|| unset meta
			}
			[ -v meta ]  \
				&& VERBOSITY_CHANNELS_META[$ch_name]='#'  \
				|| unset VERBOSITY_CHANNELS_META[$ch_name]

			__describe_how_level_changes
			[ -v level_enforced ]  \
				|| level=${VERBOSITY_CHANNELS[$ch_name]}

			VERBOSITY_CHANNELS[$ch_name]=$level
			[ -v VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name] ]  \
				&&  unset VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name]

			__describe_how_locklevel_changes
			[ -v lock_on_level_enforced ] && {
				[ -v lock_on_level ]  \
					&& VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name]=$lock_on_level  \
					|| unset VERBOSITY_CHANNELS_LOCK_ON_LEVEL[$ch_name]
			}
			;;&

		channel|meta-channel)
			__describe_how_channel_changes
			[ -v group_items ]  \
				&& VERBOSITY_CHANNELS_GROUP_ITEMS+=( [$ch_name]="$group_items" )
			;;

		group|meta-group)
			__describe_how_group_changes
			if [ -v group_items ]; then
				[ -v group_items_enforced ]  \
					&& VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]="$group_items"  \
					|| VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]+=",$group_items"
					   #  ^ We know, that both existing list and the new one
					   #    are non-empty: during the pattern check above it’s
					   #    impossible to create an empty group.
			else
				[ -v force_conversion_to_channel ]  \
					&& unset VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]
			fi
			;;

		channel\ in\ group*)
			err "Cannot create or alter “$ch_name”: it already exists as a $ch_type."
			;;
	esac

	[ -v VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name] ] && {
		declare -n group_items="VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]"
		for slave_ch_name in ${group_items//,/ }; do
			VERBOSITY_CHANNELS[$slave_ch_name]=$level
		done
	}

	(( verbosity_depth >= 3 )) && {
		mildec
		info "${to_be_chtype^} “$ch_name” successfully altered."
	}
	return 0
}
export -f __set_up_verbosity_channel


 # Adds a verbosity channel to a group.
#  $1 – group name. Must exist beforehand. May be a regular channel (an exist-
#       ing function name), in which case the name will be converted
#       to a group.
#  $2 – channel name. Should be an existing function name (or the name of
#       a function to be created shortly).
#
#  If the speed is a concern: theoretically, it’s better to avoid using this
#  function to expand group items, and instead specify all items at once at
#  the time you run “vchan setup”. In practise, there’s no significant diffe-
#  rence between 1–5 group items added separately and via “vchan setup”.
#
__add_verbosity_channel_to_group() {
	local  gr_name="$1"
	local  ch_name="$2"
	local  gr_type

	__validate_verbosity_channel_name "$gr_name"
	__validate_verbosity_channel_name "$ch_name"
	gr_type="$(__get_verbosity_channel_type "$gr_name")"
	case "$gr_type" in
		channel|meta-channel)
			VERBOSITY_CHANNELS_GROUP_ITEMS[$gr_name]="$ch_name"
			;;&

		group|meta-group)
			VERBOSITY_CHANNELS_GROUP_ITEMS[$gr_name]+=",$ch_name"
			;;&

		channel|group|meta-channel|meta-group)
			#  Yes, it’s a duplicate. For the ease of retrieving.
			#  Yes, “vchan setlevel” and “vchan setup” update
			#  this duplication when necessary.
			VERBOSITY_CHANNELS[$ch_name]=${VERBOSITY_CHANNELS[$gr_name]}
			[ -v VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name] ]  \
				&& unset VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name]
			;;

		channel\ in\ group*)
			[[ "$gr_type" =~ “([A-Za-z_][A-Za-z0-9_-]*)” ]]
			err "Cannot add channel “$ch_name” to group “$gr_name”:
			     it itself belongs to another group “${BASH_REMATCH[1]}”."
			;;

		'doesn’t exist')
			err "Cannot add channel “$ch_name” to group “$gr_name”:
			     no group or channel with such name exists."
			;;
	esac

	return 0
}
export -f __add_verbosity_channel_to_group


 # Displays all known verbosity channels.
#
__list_verbosity_channels() {
	local  ch_names=()
	local  ch_name
	local  ch_level
	local  ch_type
	local  attrs
	local  group_items

	info 'List of known verbosity channels:'

	for ch_name in "${!VERBOSITY_CHANNELS[@]}"; do
		ch_type=$(__get_verbosity_channel_type "$ch_name")
		[[ "$ch_type" =~ ^(meta-|)(channel|group)$ ]]  \
			&& ch_names+=( "$ch_name" )
	done

	echo
	msg "$( while read ch_name; do
	            ch_level=${VERBOSITY_CHANNELS[$ch_name]}
	            attrs='' highlight=''
	            [ -v VERBOSITY_CHANNELS_META[$ch_name] ]  \
	                && attrs+='#'  \
	                || attrs+=' '
	            [ -v VERBOSITY_CHANNELS_GATES_AN_OUTPUT[$ch_name] ]  \
	                && attrs+='*'  \
	                || attrs+=' '
	            [ -v VERBOSITY_CHANNELS_SUSPICIOUS[$ch_name] ]  \
	                && attrs+='?'  \
	                || attrs+=' '
	            group_items=${VERBOSITY_CHANNELS_GROUP_ITEMS[$ch_name]:-}
	            echo -e "$ch_level\t$attrs\t$ch_name\t$(sed -r 's/,/, /g' <<<"$group_items")"
	        done < <(sort < <(IFS=$'\n'; echo "${ch_names[*]}"))  \
	                | column -t -s $'\t' -N "Level,Attr,Name,Slave channels"  \
	                | sed -r "s/^([0-9]+)(\s+#?\*\s+)/${__so}\1${__s}\2/g
	                          s/\?/${__bri}${__r:-}\?${__s}/g"
	                #  ^ Highlighting the levels of channels, that gate
	                #    outputs, AFTER “column”, because “column” can’t
	                #    process terminal control sequences properly.
	      )"
	echo
	msg 'Attr.: * – levels gate an output (not messages).
	            # – channel name won’t match function names (except slaves).
	            ? – possibly a mistyped name: no module claimed it.

	     To get information about a channel, run with --help-verbosity=CHANNELNAME'

	return 0
}
export -f __list_verbosity_channels


 # Universal command to work with verbosity channels, levels and groups.
#
#  Any code outside of this file should operate on VERBOSITY_CHANNELS
#  through the means of vchan. It’s simpler and safer this way. The vchan
#  commands avoid duplication and prevent common mistakes like assigning
#  to a non-existing channel or a futile attempt to change log level.
#
vchan() {
	local  command="${1:-}"
	local  arg="${2:-}"
	local  arg2="${3:-}"
	local  ch_name
	local  level
	local  min_level
	local  cur_level
	local  eq_level
	local  verbosity_depth=${VERBOSITY_CHANNELS[Verbosity]}

	case "$command" in
		getname)
			__get_current_verbosity_channel_name
			;;

		getlevel)
			if [ "$arg" ]; then
				__validate_verbosity_channel_name "$arg"
				ch_name="$arg"
			else
				ch_name=$(__get_current_verbosity_channel_name)
			fi
			echo "${VERBOSITY_CHANNELS[$ch_name]}"
			;;

		setlevel)
			if [ "$arg" -a "$arg2" ]; then
				__validate_verbosity_channel_name "$arg"
				ch_name="$arg"
				level="$arg2"

			elif [ "$arg"  -a  -z "$arg2" ]; then
				ch_name=$(__get_current_verbosity_channel_name)
				level="$arg"
			fi
			__validate_verbosity_channel_level "$level"
			__set_verbosity_level "$ch_name" "$level"
			;;

		iflevel)
			if [ "$arg" -a "$arg2" ]; then
				__validate_verbosity_channel_name "$arg"
				ch_name="$arg"
				eq_level="$arg2"

			elif [ "$arg"  -a  -z "$arg2" ]; then
				ch_name=$(__get_current_verbosity_channel_name)
				eq_level="$arg"
			fi
			__validate_verbosity_channel_level "$eq_level"
			cur_level="${VERBOSITY_CHANNELS[$ch_name]}"

			(( cur_level == eq_level ))  \
				&& return 0  \
				|| return 1
			;;

		minlevel)
			if [ "$arg" -a "$arg2" ]; then
				__validate_verbosity_channel_name "$arg"
				ch_name="$arg"
				min_level="$arg2"

			elif [ "$arg"  -a  -z "$arg2" ]; then
				ch_name=$(__get_current_verbosity_channel_name)
				min_level="$arg"
			fi
			__validate_verbosity_channel_level "$min_level"
			cur_level="${VERBOSITY_CHANNELS[$ch_name]}"

			(( cur_level >= min_level )) \
				&& return 0  \
				|| return 1
			;;

		gettype)
			if [ "$arg" ]; then
				__validate_verbosity_channel_name "$arg"
				ch_name="$arg"
			else
				ch_name=$(__get_current_verbosity_channel_name)
			fi
			__get_verbosity_channel_type "$ch_name"
			;;

		setup)
			#  Specification has too many conditions. They could be here, but
			#  then they’d have to be duplicated in the __create…() function.
			#  Passing the spec as is is the shortest way to deal with this.
			__set_up_verbosity_channel "$arg"
			;;

		addtogroup)
			__add_verbosity_channel_to_group "$arg" "$arg2"
			;;

		list)
			__list_verbosity_channels ${arg:-}
			;;

		help)
			cat <<-EOF
			vchan – set of commands to control verbosity channels.

			    vchan getname
			    vchan getlevel [channel_name]
			    vchan setlevel [channel_name] <level>
			    vchan iflevel [channel_name] <eq_level>
			    vchan minlevel [channel_name] <min_level>
			    vchan gettype [channel_name]
			    vchan setup <[!][#]name[!][:[!]name1[,name2,…,nameN]]>[=[!]<level>][|[!]<lock-level>]
			    vchan addtogroup <group_name> <channel_name>
			    vchan list [be_verbose]

			Call “vchan help-COMMAND” to display help for a particular COMMAND.
			EOF
			;;

		help-getname)
			cat <<-EOF
			vchan getname
			    Retrieves the name of the verbosity channel, that would be
			    applied at this point in code.

			    The current verbosity channel is determined by checking the
			    call stack and comparing all function names, starting from
			    the caller of “vchan” and up to the root. The first function
			    name, that directly matches a channel name or is mentioned as
			    a group item in a verbosity channel, determines the verbosity
			    level at the current point.

			    The root is always a function named “main” (automatically
			    created by Bash), so the “main” channel created by Bahelite
			    catches all functions by default.

			    “vchan” is designed to disallow ambiguity and doesn’t allow
			    to create a channel, if that name already appears as a group
			    item in another channel – and vice versa.
			EOF
			;;

		help-getlevel)
			cat <<-EOF
			vchan getlevel [channel_name]
			    Retrieves the level of the specified channel name or, if it
			    isn’t specified, the channel that is currently in effect.
			EOF
			;;

		help-setlevel)
			cat <<-EOF
			vchan setlevel [channel_name] <level>
			    Sets the level of the specified channel name or, if no name
			    is specified, change the verbosity level of the channel that is
			    currently in effect. Level must be a number in range 0…99999.
			EOF
			;;

		help-iflevel)
			cat <<-EOF
			vchan iflevel [channel_name] <number>
			    Returns true, if verbosity level of the channel exactly matches
			    the specified number – and false, if they don’t match. Number must
			    be in range 0…99999. If no channel name is specified, current
			    channel is assumed.
			EOF
			;;

		help-minlevel)
			cat <<-EOF
			vchan minlevel [channel_name] <number>
			    Returns true, if verbosity level of the channel is equal
			    or greater than the specified number. Number must be in range
			    0…99999. If channel name is not specified, current channel
			    is assumed.
			EOF
			;;

		help-gettype)
			cat <<-EOF
			vchan gettype [channel_name]
			  Retrieves the type of the specified channel:

			    (meta-)channel
			        The name refers to an ordinary channel.

			        Prefix “meta-” means that the channel name will not match
			        against any function names. For example, when function foo()
			        wishes to determine which verbosity channel it belongs to,
			        it queries \`vchan getname\`. It will match against channel
			        named “foo” (because it’s non-meta), but won’t match against
			        channel “#foo” (the hash sign stands for “meta”).

			    (meta-)group
			        The name is a bundle name, and the level associated with
			        it affects a group of function names. The prefix works
			        like for the channels.

			    channel in group “GROUPNAME”
			        This string is returned when the specified name happens to be
			        a function name within another group (they cannot be channels
			        of their own). GROUPNAME will be replaced with an actual
			        group name.

			    doesn’t exist
			        This string is returned when the specified name doesn’t
			        match any channel or group name.
			EOF
			;;

		help-setup)
			cat <<-EOF
			vchan setup <[!][#]name[:[!]name1[,name2,…,nameN]]>[=[!]<level>]
			Sets up a verbosity channel (channel may be a group). Setting up
			includes:
			  - creation;
			  - switching the “meta” attribute;
			  - adding group items;
			  - changing the level;
			  - converting a channel to a group or stripping a group of its
			    items, turning it back to an ordinary channel.

			Examples for channels, groups, levels and meta

			    ui
			        Creates an ordinary channel “ui” with the default level “$VERBOSITY_DEFAULT_LEVEL_FOR_NEW_CHANNELS”.
			        Will match function with the name “ui” and affect verbosity
			        of its messages.

			    ui=5
			        Same as above, but will set the verbosity level to “5”.

			    ui:func1,func2
			        Creates a group “ui”. Functions “func1”, “func2” and “ui”
			        will use the same verbosity level (here it’s not set, so
			        defaults to “3”).

			    #ui
			        Creates a meta-channel with the default level “3”. Meta-
			        channels do not match against function names, so for the
			        main script there’s no need to create those.
			        See \`vchan help-gettype\` for details.

			    #ui:func1,func2
			        Creates a group with a “meta” name. Unlike the items of that
			        group, its name will not match against any existing function
			        names.


			    Note: verbosity channel associated with a function name will
			    also affect all the functions called from inside that one,
			    whose name matches with the channel name. See the output of
			    “vchan help-getname” to get information on how the current
			    channel is determined.


			Examples for enforced and yielding parts

			    It’s convenient to be able to change verbosity levels from
			    the outside, for example, like
			        VERBOSITY=log=0,console=1 ./myscript.sh
			    but it results it that the channels have to be created before
			    the modules that they are associated with, are loaded. And those
			    modules should be able to either create or update the verbosity
			    channel as necessary. Thus the specification to \`vchan setup\`
			    command can either enforce something or let this part yield
			    to a predefined setting.

			    Prepend a part of specification with the ! sign to enforce
			    setting this part verbatim.

			    ui
			        “Weak” definition, preserves all predefined settings, that
			        there may be.

			    !ui
			        Enforces: non-meta type (note the absence of ‘#’)
			        Preserves: the level; the group definition (if the name
			        was predefined as a group).

			    ui!
			        Enforces: that the name must be an ordinary channel.
			        Preserves: meta/non-meta, level.

			    ui=!0
			        Enforces: level “0”
			        Preserves: meta/non-meta; channel/group (with group items
			        if it was predefined as a group).

			    !#ui:!func1:func2=!5
			        Preserves nothing. Enforces that the name “ui” would refer
			        to a group with a meta-name “ui”, and that the group should
			        have only two items “func1” and “func2” and have level 5.

			    !ui!
			        Enforces, that the name “ui” would refer to an ordinary,
			        non-meta channel. If it was predefined as a group, then
			        group items are discarded (note the absence of group spec).
			        Preserves level.

			        Note: “name!” won’t work, if group items are specified
			        (that includes their colon), e.g. like “name!:func1,func2”
			        or “name!:!func1,func2”. Group items in the same statement –
			        enforced or not – will prevent conversion to an ordinary
			        channel.

			    ui:func3,func4
			        A special case: expansion. If “ui” is a channel, it is
			        converted to group and these items are assigned to it.
			        If “ui” is already a group, “func3” and “func4” are added
			        at the end of its item list.

			        (This example, as well as the others, may be combined with
			        other types of enforcement.)


			“name” is an alphanumeric string, that:
			    - can contain underscores, can start with an underscore;
			    - can contain hyphens, but cannot start with a hyphen;
			    - cannot start with a number.
			“level” is a number in range 0…99999.
			EOF
			;;

		help-addtogroup)
			cat <<-EOF
			vchan addtogroup <group_name> <channel_name>
			    Add the specified channel to a group. The channel name cannot
			    be itself a group or already an item in an existing group.
			    The destination group name must exist (may be a group, may
			    be a regular channel – in which case it will be expanded
			    to become a group).

			    As the “group” is what hangs in mind after typing the command,
			    the first argument is a group and the channel goes after.
			EOF
			;;

		help-list)
			cat <<-EOF
			vchan list [be_verbose]
			    List all known verbosity channels. If “be_verbose” is used,
			    also lists their levels.
			EOF
			;;

		*)
			err "vchan: no such command: “$command”.
			     Try “vchan help”."
			;;
	esac
	return 0
}
export -f  vchan



                         #  Stream control  #

 # Remembering the original FD paths. They are needed to send info, warn etc.
#  messages from subshells properly.
#
if (( BASH_SUBSHELL == 0 )); then
	declare -gx STDIN_ORIG_FD_PATH="/proc/$$/fd/0"
	declare -gx STDOUT_ORIG_FD_PATH="/proc/$$/fd/1"
	declare -gx STDERR_ORIG_FD_PATH="/proc/$$/fd/2"
else
	[ -v STDIN_ORIG_FD_PATH ]   \
		|| declare -gx STDIN_ORIG_FD_PATH="/proc/$$/fd/0"
	[ -v STDOUT_ORIG_FD_PATH ]  \
		|| declare -gx STDOUT_ORIG_FD_PATH="/proc/$$/fd/1"
	[ -v STDERR_ORIG_FD_PATH ]  \
		|| declare -gx STDERR_ORIG_FD_PATH="/proc/$$/fd/2"
fi


 # Saving the original destinations of console stdout and stderr,
#  so that they may be used to bypass writing some temporary information
#  like a progressbar to the log file, and also for the logging module
#  to have these in case console verbosity would redirect stdout and stderr
#  to /dev/null.
#
exec {STDOUT_ORIG_FD}>&1
exec {STDERR_ORIG_FD}>&2

case "${VERBOSITY_CHANNELS[console]}" in
	0)	exec 1>/dev/null
		exec 2>/dev/null

		 # If the logging module would need to log stdout, it would need
		#  to know, that it must grab the handle not from FD 1, but from
		#  FD $STDOUT_ORIG_FD. Same for stderr.
		#
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL=t
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDERR_TO_DEVNULL=t
		;;

	1)	exec 1>/dev/null
		BAHELITE_CONSOLE_VERBOSITY_SENT_STDOUT_TO_DEVNULL=t
		;;
esac



return 0



 # HOW THE CURRENT VERBOSITY LEVEL IS DETERMINED
#
#  The algorithm, that checks the verbosity level for a particular function,
#  performs search in the following way:
#    - if there is a channel with this exact name, its level is used;
#    - if there is no such channel, the search goes up on the call stack,
#      (FUNCNAME[@]), until some function would match a non-meta verbosity
#      channel (an item in VERBOSITY_CHANNELS[@]). At the bottom of FUNCNAME
#      there is always a function “main”, that matches the always-present
#      verbosity channel “main”, which would define the verbosity level
#      for the function, if no other channel would.
#



 # TIPS ON THE USE
#
#  If you create a verbosity channel, you probably want to reduce its output.
#  Consider hiding all “info” or even “info” and “warn” messages by default
#  by setting the verbosity level to 2 or to 1 for the new channel. This way
#  you can use these messages without any -level suffixes (e.g. -4, -5) to
#  functions. If that part of code must show output by default, using level 4
#  is inevitable.
#
#  Intended use of self-report verbosity levels:
#  0 – quiet, no messages
#  1 – error-level messages only
#  2 – errors and warnings
#  3 – errors, warnings and info messages (including abort)
#  4..n – verbose messages.
#
#  The main script can define and use any level up to 99999. (See the comment
#  over the “__msg” function in the “messages” module.)
#
#  You may create channels with short names, and specify long names of actual
#  functions as the group items of that channel.
#



 # LIMITATIONS
#
#  Supposing that there could be two functions bearing the same name, but
#  existing within two different modules (thus having differnet FUNCNAME
#  lists), and there would be a verbosity channel listing one of those
#  functions by its name, this may lead to unforseen consequences.
#
#  Unless you know each command from the verbosity module in the face, it’s
#  advised that you use “vchan” command for all operations in the main script.
#  An exception may be made for requesting the level of a particular channel
#  with “${VERBOSITY_CHANNELS[channel_name]}”. (But honestly, there is no
#  reason to go deeper than vchan – the code is optimised for querying levels.)
#
#  You may not have function named “main” in the main script. To have it is
#  generally a bad idea anyway, because Bash uses this function name as a
#  global root of the execution pipeline – and for this reason it’s always
#  present in the FUNCNAME array. So if you introduce function “main”, you are
#  asking for problems. Now, to have a function “main” will not lead to an
#  error, but don’t expect that it alone will be using the channel “main”.
#



 # ON THE WAY THIS MODULE IS WRITTEN
#
#  The foremost concern is fast retrieval of the verbosity levels.
#  This is dictated by the following chain of reasons:
#
#  1. The code must be clean
#     It is desired to handle verbosity without placing explicit conditions
#     that would check verbosity level and then run message function. That
#     means, instead of this
#
#         func1() {
#             local verbosity_depth=${VERBOSITY_CHANNELS[Postload_jobs]}
#             (( verbosity_depth >= 3 ))  && {
#	              info "Func1 started."
#                 milinc
#             }
#             …
#             …
#             …
#             return 0
#         }
#
#     or this…
#
#         func1() {
#             local verbosity_depth=$(vchan getlevel Postload_jobs)
#             (( verbosity_depth >= 3 ))  && {
#	              info "Func1 started."
#                 milinc
#             }
#             …
#             …
#             …
#             return 0
#         }
#
#     or this…
#
#         func1() {
#             …
#             vchan minlevel Postload_jobs 4 && {
#	              info "Func1 started."
#                 milinc
#             }
#             …
#             …
#             vchan minlevel Postload_jobs 4  \
#	              && info "Func1 started."
#             …
#             …
#             vchan minlevel Postload_jobs 5  && {
#                 milinc
#	              info "Func1 started."
#                 mildec
#             }
#             …
#             return 0
#         }
#
#     it should look like this:
#
#         func1() {
#	          info "Func1 started."
#             milinc
#             …
#             …
#             …
#             return 0
#         }
#         vchan addtogroup func1 Funcs
#
#     and like this:
#
#         func1() {
#             …
#             info-4 "Func1 started."
#             milinc-4
#             …
#             …
#             info-4 "Func1 started."
#             …
#             …
#             milinc-5
#	          info-5 "Func1 started."
#             mildec-5
#             …
#             return 0
#         }
#         vchan addtogroup func1 Funcs
#
#     In other words, the verbosity-obeying code shouldn’t stand out, wrapped
#     in something, it should look like an ordinary code and when reading
#     the code, the verbosity involved should be transparent to the reader,
#     there shouldn’t be heavy conditional structures that clog the code.
#
#
#  2. The code must be easily understandable
#
#
#  3. The code must be fast.
#
#     Creating or changing settings may take a time, but the retrieval must
#     be as fast as possible, because to keep the code as condition-less
#     as possible, the conditions must be imbued in the message functions.
#     This means, that they may do a big number of checks per function.
#     If these checks would take a big time, it will make the code slow.
#     So they must be done as fast as possible, but not huring the code
#     readability.
#
#
#  In attempt to balance the three aspects of the desirable code, it was
#  rewritten three times. After the second rewrite it has become clear, where
#  lie the bounds of theoretically possible for each aspect, and where lie the
#  the limits of what can be actually implemented, so as that wouldn’t harm
#  the implementation of the other two principles.
#
#
#  For example, the fastest way would be referring to the variables directly,
#  that is, to query levels directly from variables. However, that was bound
#  to several difficulties:
#    - to retrieve the level of a channel, that is a group item of another
#      channel, it is better made a symbolic link to that channel’s variable,
#      holding the level;
#    - but it’s not possible for an array element to be a symbolic link to
#      another element (as of bash 5.0 at least);
#    - That means, that if group items are to return their levels fast, all
#      verbosity levels should be represented as a standalone variable, not
#      grouped under a name of an array, i.e. there would be a variable
#          VCHAN_console=1
#      and some other channel
#          VCHAN_group_item_of_console=<links to console>
#
#      and if you think of it, this style kinda works, everything channel-
#      related may be atomised with variables, e.g. the meta attribute
#      may be defined as
#          VCHAN_IS_META_console=t
#      and then if it’d be necessary to check if the channel is meta (i.e. is
#      allowed to match function name or not), it would take only to check if
#      a corresponding variable with the channel’s name does exist.
#
#      However, this all has a big drawback: in the code it will look confusing
#      and poorly readable. VERBOSITY_CHANNELS[console] gives a better under-
#      standing that you are working with a set of something, and you see
#      clearly, where is a common name and where is the name of an item.
#      With VCHAN_chan_name  this remains too vague. Is this a one-of-a-kind
#      or there are more? If I use VCHAN_ prefix for another channel name,
#      will it even work, or there may be some VCHANSPECIALSUFFIX_? This
#      vagueness in the bounds is bad and hacky.
#    - There is, however, another solution, that would allow to maintain both
#      the clarity of an array and ease of access: good old data duplication.
#      Yes, basically on the creation of a group item it will copy it’s master
#      channel’s level to itself, and when queried, it will respond with
#      a number as fast as any other channel (no seeking or parsing group items
#      will be required). The drawbacks are minimal:
#        - as group items will not be symbolic links, we can’t find them
#          at runtime, this list has to be stored somewhere (but it’d be acces-
#          sed only on set/change operations which are allowed to be “heavy”,
#          this doesn’t harm the speed of reading the data);
#        - on set/change, the function performing the job must update the level
#          for all underlings (but again, this action is normally done only
#          once and isn’t harmful for reading speed);
#        - this is less fool-proof, and we’ll have to depend on the helper
#          functions from vchan, so that they would validate everything.
#          A sample case would be a user (author of a main script), who changes
#          the level of a slave verbosity channel directly via its variable
#          in the VERBOSITY_CHANNEL array, expecting that this would affect
#          the master channel. However, this is not a serious issue, since
#          vchan will remain to be around anyway to provide a slow but bullet-
#          proof solution. Moreover, this relates to the creating/changing
#          levels, the one-time and slow operations, for which vchan will
#          ALWAYS be used (as the speed is not a concern here, there’s no
#          reason to not use vchan.)
#
#  The second redaction of the verbosity module was already fast enough, if it
#  used the “smart” way of operating with verbosity. At that point it has become
#  clear, that there will be two ways of checking verbosity level:
#    - **with clear code,** that relays checking verbosity on the message
#      functions.
#    - **with cached verbosity,** that retrieves the current verbosity level
#      once at the start of the function, and then uses an arithmetic condition
#      to run or not run verbosity-related code.
#  The second method is the fastest, because comparing numbers is done nearly
#  this instant (unlike with the other method, that involves several function
#  calls, calls to subshells and their internal processing).
#     In some cases thoughout Bahelite it made sense to just query the channel
#  level directly from the variable. Querying levels directly has begun to
#  seem like a viable method. With an assumption, that the author of the main
#  script knows his code and queries levels when they were 100% created
#  beforehand, this proves to be the best – both readable and fast – way to
#  query verbosity (in certain situations).
#     However, storing verbosity level in the function for later arithmetic
#  comparisons is somewhat clogging the code, so the motivation for the third
#  rewrite was to make the other way, with the clean code, as fast as possible,
#  but not to harm readability.
#
#  The verbosity internals of the second redation lacked speed because of
#  the compact way of storing data. This resulted in the necessity to search
#  within strings and iterate. Thus, the primary aim was placed upon removing
#  search and iteration from the procedures involved in the retrieval of the
#  data. The retrieval should be
#    - as close to reading a varaible as possible;
#    - may involve function calls, but should try to avoid subshell calls,
#      when possible. And with function calls, use less of them too.
#
#  Fortunately, it was possible to reimplement the internals, having the
#  necessity to each higher speed in mind. This involved intentional dupli-
#  cation of data and steps that might seem questionable to anyone at first
#  sight. But after reading this you must know, that everything is made so
#  for a reason.
#
#
#  P.S. To outline some omitted points:
#    - retrieving verbosity channels means in many cases “to determine the
#      current verbosity channel’s name and return its level”. And this action
#      involves not only discerning between actual channels and those that
#      depend on them (aka “group items”), but also discerning between meta
#      and non-meta channels, i.e. those whose names cannot and can match
#      against function names, when searching for the current verbosity chan-
#      nel. Discerning quick between meta and non-meta thus also has to be
#      done so as to be quick, and this is why the channel attributes, such as
#      “meta” and “lock after level” are implemented the single-variable way.
#      (With those it seemed reasonable, since they remain internal variables,
#      never exposed to user, and with no need to user to use them directly,
#      as that doesn’t affect execution speed.)
#
