#  Should be sourced.

#  github.sh
#  For the projects hosted on Github, allows to perform a check for new
#  release each severeal days and display a notification to the user.
#  It doesn’t download or update code of the program.
#  © deterenkelt 2018–2020

#  Require bahelite.sh to be sourced first.
[ -v BAHELITE_VERSION ] || {
	cat <<-EOF  >&2
	Bahelite error on loading module ${BASH_SOURCE##*/}:
	load the core module (bahelite.sh) first.
	EOF
	exit 4
}

#  Avoid sourcing twice
[ -v BAHELITE_MODULE_GITHUB_VER ] && return 0
bahelite_load_module 'versioning' || exit $?
#  Declaring presence of this module for other modules.
declare -grx BAHELITE_MODULE_GITHUB_VER='1.0.13'
BAHELITE_INTERNALLY_REQUIRED_UTILS+=(
#	date      # (coreutils)
#	stat      # (coreutils)
	ps        # (procps)
	wget      # (wget)
	xdg-open  # (xdg-utils)
)
BAHELITE_INTERNALLY_REQUIRED_UTILS_HINTS+=(
	[ps]='ps is a part of procps-ng.
	http://procps-ng.sourceforge.net/
	https://gitlab.com/procps-ng/procps'
	[xdg-open]='xdg-open belongs to xdg-utils
	https://www.freedesktop.org/wiki/Software/xdg-utils/'
)

__show_usage_github_module() {
	cat <<-EOF
	Bahelite module “github” options:

	latest_release_url=${__so}URL${__s}
	    An URL like “https://github.com/deterenkelt/Bahelite/releases/latest”.
	    Requried option.

	    The HTML page is downloaded with wget (<70 KiB), parsed to retrieve
	    the latest available version and then compare it to the current
	    version of the program (i.e. the main script).

	release_check_interval=${__so}0…9999${__s}
	    Amount of days between checks. The default is 21.

	release_notes_url=${__so}URL${__s}
	    When a check finds a new release, it may add the link to release
	    notes to the console notification. (Desktop message remains short
	    and displays only “A new version is available” and the new version.)

	EOF
	return 0
}


declare -gx GITHUB_LATEST_RELEASE_URL
declare -gx GITHUB_RELEASE_NOTES_URL
declare -gx GITHUB_RELEASE_CHECK_INTERVAL=21

for opt in "$@"; do
	if [ "$opt" = help ]; then
		__show_usage_github_module
		exit 0

	elif [[ "$opt" =~ ^latest(_release_|-release-)url=https?://github\.com/.+$ ]]; then
		GITHUB_LATEST_RELEASE_URL="$opt"

	elif [[ "$opt" =~ ^release(_notes_|-notes-)url=https?://.*github\.com/.+$ ]]; then
		GITHUB_RELEASE_NOTES_URL="$opt"

	elif [[ "$opt" =~ ^release(_check_|-check-)interval=([0-9]|[1-9][0-9]{1,3})$ ]]; then
		GITHUB_RELEASE_CHECK_INTERVAL="${BASH_REMATCH[2]}"

	else
		redmsg "Bahelite: github: unrecognised option “$opt”."
		__show_usage_load_modules_module
		exit 4
	fi
done
unset opt

[ -v GITHUB_LATEST_RELEASE_URL ]  \
	|| err 'Specify the “latest_release_url” option to the github module
	        to do a check for updates!'

 # Downloads “Releases” page of a github repo and compares the version
#  of the latest release to the current version of the program.
#  This function compares version with compare_versions(), so it also
#  works only with maximum three-numbered versions (X, X.Y or X.Y.Z)
#
#  TAKES
#    $1  – version string to compare with the latest release.
#
#  RETURNS
#    0 if there’s a new release, 1 if not, 5 in case of error in retrieving
#    or parsing input.
#
check_for_new_release() {
	bahelite_xtrace_off  &&  trap bahelite_xtrace_on RETURN
	#  MYDIR is checked here for locally downloaded and locally launched
	#  scripts! If you install to OS, consider using CACHEDIR!
	local timestamp_file="${CACHEDIR:-$MYDIR}/updater_timestamp"
	[ -f "$timestamp_file" ] || touch "$timestamp_file"

	local days_since_last_check=$((	(   $(date +%s)
	                                  - $(stat -L --format %Y "$timestamp_file")
	                                )
	                                / 60
	                                / 60
	                                / 24
	                             ))

	(( days_since_last_check < GITHUB_RELEASE_CHECK_INTERVAL ))  \
		&& return 0

	local  our_ver="$1"
	local  latest_release_ver
	local  message
	local  open_relnotes_url
	local  msgfunc

	is_version_valid "$our_ver" || {
		warn "Main script version “$our_ver” is not a valid version string."
		return 5
	}
	latest_release_ver=$(
		shopt -s extglob
		user_and_repo=${GITHUB_LATEST_RELEASE_URL#*github.com/}
		user_and_repo=${user_and_repo%%+(/)}
		user_and_repo=${user_and_repo%/releases/latest}
		wget -O- "$GITHUB_LATEST_RELEASE_URL"  \
			|& sed -rn "s=^.*/$user_and_repo/tree/v([0-9\.]+).*$=\1=p;T;Q"
	) || true
	is_version_valid "$latest_release_ver" || {
		warn "Latest release version “$latest_release_ver” is not a valid version string."
		return 5
	}
	touch "$timestamp_file"

	if compare_versions "$latest_release_ver" '>' "$our_ver"; then
		[ "$(type -t info-ns)" = function ]  \
			&& msgfunc=info-ns  \
			|| msgfunc=info
		$msgfunc "${__bri}v$latest_release_ver is available!${__s}"
		milinc
		[ -v GITHUB_RELEASE_NOTES_URL ] && {
			# case "$relnotes_action" in
				# ask_to_open)
					# message="Would you like to read release notes\n"
					# message+="for v$latest_release_ver on Github?"
					#  If our shell has a terminal…
					#  (literally: it is a foreground process)
					#  P.S. no, both [[ "$-" =~ ^.*i.*$ ]] and [ -t 0 ]
					#       do not work here.
					# if [[ "$(ps -o stat= -p $$)" =~ ^.*\+.*$ ]]; then
					# 	menu "${message//\\n/}" Yes No
					# 	[ "$CHOSEN" = Yes ] && open_relnotes_url=t
					# else
					# 	which Xdialog &>/dev/null && {
					# 		local dialog=Xdialog
					# 		bahelite_errexit_off
					# 		$dialog --stdout \
					# 	            --ok-label Open \
					# 	            --cancel-label No \
					# 	            --yesno "$message" 400x110 \
					# 			&& open_relnotes_url=t
					# 		bahelite_errexit_on
					# 	}
					# fi
					# ;;
				# open)
				# 	open_relnotes_url=t
				# 	;;
				# print)
					msg "You can read, what’s new in the new version here:
					      $relnotes_url"
					# ;;
				# *)
					#  Do nothing.
					# ;;
			# esac
			# [ -v open_relnotes_url ] && xdg-open "$relnotes_url"
			mildec
		}
	else
		info 'This version is the latest available.'
		return 0
	fi
	return 0
}
export -f  check_for_new_release



return 0