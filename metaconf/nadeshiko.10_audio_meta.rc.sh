#  nadeshiko.10_audio_meta.rc.sh



 # Helps to determine, when it makes sense to use extra bitrate for audio.
#
#  * DTS-HD MA is reported in ffprobe as such, but in “mediainfo -f
#    --output=XML” (which performs the check on lossless format) it is
#    reported as “DTS” format with “MA” profile.
#
known_audio_lossless_formats=( FLAC PCM TrueHD DTS:MA )


 # The character which, if found in the format string, would indicate
#  that a specific format profile is required (should go after the
#  delimiter).
#
known_audio_lossless_formats_profile_delimiter=':'