#  nadeshiko.10_subtitles_meta.rc.sh



 # The list of subtitle formats, that can be hardsubbed.
#  Used in the stream check at initial stage.
#
known_sub_codecs=(
	ass
	  ssa
	srt
	  subrip
	  webvtt
	  vtt
	mov_text
	dvd_subtitle
	hdmv_pgs_subtitle
)